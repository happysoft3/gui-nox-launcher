
#include "head.h"

#ifndef NoxLadderClass
#define NoxLadderClass


#define NOX_SERVER_KILL			0x6542d4
#define NOX_SERVER_DEATH		0x6542d8
#define NOX_CLIENT_KILL			0x62fa30
#define NOX_CLIENT_DEATH		0x62fa34

#define NOX_CGAME_FLAG			0x750454


typedef struct _ladder
{
	unsigned char isServer;
	unsigned char ladderEnable;
	int curKills;
	int curQDeath;
	int curDeath;
	int curGame;
	int curVictory;
	int allKills;
	int allDeath;
	int allGens;
	int allSecret;
	int allMons;
	int allQDeath;
	int allGameCount;
	int allVictory;
	int allDefeat;
	char userId[12];
	int killOffset[2];
	int deathOffset[2];
	unsigned char isQuestMode;
	int getLevel;
	PMASK *mainProc;
}LadderClass;

#endif

void GameLoopOnJoinToServer(PMASK *pmask);
void GameLoopOnExitToServer(PMASK *pmask);
void GameLoopOnGame(LadderClass *ladder);

void ShowLadderStat(LadderClass *ladder, char *dest, int destLen);

void PrintLadderStat(LadderClass *ladder, int say);