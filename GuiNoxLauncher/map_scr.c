
#include "head.h"
#include "map_scr.h"
#include "tools.h"

void NoxScriptMain(PMASK *pmask)
{
	FILE *fp;
	char map_name[NOX_EXE_NAME_LEN], local[200], scr_fn[20];

	ReadProcessMemory(pmask->pground, (LPCVOID)0x753A6C, &map_name, sizeof(map_name), NULL);
	if (map_name[0])
	{
		if (strcmp(map_name, pmask->map_name_buff))
		{
			strcpy_s(pmask->map_name_buff, NOX_EXE_NAME_LEN, map_name);
			sprintf_s(local, sizeof(local), "%s%s.obj", pmask->nox_path, map_name);
			sprintf_s(scr_fn, sizeof(scr_fn), "%s.obj", map_name);
			fopen_s(&fp, local, "rb");
			if (fp != NULL)
			{
				WriteMemString(pmask, 0x751060, scr_fn, 20);
				SetMemory(pmask, 0x505376, 0x751060);
				fclose(fp);
			}
			else
				SetMemory(pmask, 0x505376, 0x5bf1e8);
		}
	}
}