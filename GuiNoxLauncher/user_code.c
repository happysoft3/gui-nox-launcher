
#include "head.h"
#include "user_code.h"
#include "tools.h"



void ExecUserCodeFile(PMASK *pmask, EDIT_TXT *edit_str)
{
	PARSE_CODE parse = { 0, {0, } };
	FILE *fp = NULL;
	char read[200] = { 0, };

	fopen_s(&fp, USER_CODE_FILE_NAME, "rt");
	if (fp != NULL)
	{
		while (fgets(read, sizeof(read), fp) != NULL)
		{
			if (!ReadCode(&parse, read))
			{
				WriteProcessMemory(pmask->pground, (LPVOID)parse.data[0], &parse.data[1], sizeof(DWORD), NULL);
			}
		}
		NoxScreenshotNameToTime(pmask);
		AppendText(edit_str, L"코드파일이 로드되었습니다\r\n");
		fclose(fp);
	}
}

void DisplayCurTimeSetup(PMASK *pmask, int target)
{
	BYTE code[5] = { 0xb8, };
	int *addr = (int *)(code + 1);

	*addr = TIME_DISPLAY_ADDRESS;
	
	WriteMemString(pmask, target, (char *)code, 5);
}

void NoxScreenshotNameToTime(PMASK *pmask)
{
	char str[100] = { 0, }, arr[5] = { 0xb8, };
	int *pic = (int *)(arr + 1);
	SYSTEMTIME systime;

	GetLocalTime(&systime);
	SetMemory(pmask, 0x46db60, 0x6dcbd068);
	
	wsprintfA(str, "%d%02d%02d%02d%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute);
	WriteMemString(pmask, 0x6dcbd0, str, strlen(str));

	DisplayCurTimeSetup(pmask, 0x437028); //added
}

int ReadCode(PARSE_CODE *parse, char *read)
{
	int max = strlen(read), res = -2;
	parse->idx = 0;
	parse->read_token = 1;

	for (int k = 0; k < max; k++)
	{
		switch (read[k])
		{
		case '#':
			return 1;
		case '0':
			if (read[k + 1] == 'x')
			{
				sscanf_s(read + k, "%x", &parse->data[parse->idx]);
				parse->idx = (parse->idx + 1) % 2;
				parse->read_token = 0;
				res++;
				k++;
				break;
			}
		case '1': case '2': case '3': case '4': case '5': case '6':
		case '7': case '8': case '9':
			if (parse->read_token)
			{
				sscanf_s(read + k, "%d", &parse->data[parse->idx]);
				parse->idx = (parse->idx + 1) % 2;
				parse->read_token = 0;
				res++;
			}
			break;
		case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
			break;
		default:
			parse->read_token = 1;
		}
	}
	return res;
}