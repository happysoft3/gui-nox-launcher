

#ifdef UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#include <process.h>
#include <Windows.h>
#include <windowsx.h>
#include <stdio.h>
#include <WinInet.h>
#include <time.h>

#include <Commctrl.h> //added


#pragma comment(lib, "wininet.lib")



#ifndef NoxPlugins
#define NoxPlugins


#define NOX_EXE_NAME_LEN					40 //20
#define NOX_PATH_LEN						200
#define MAX_EDIT_CONTENTS					1000
#define LAUNCHER_FILE_NAME					"noxpath.rul"
#define USER_SETTING_FILE_NAME				"UserSetting.csv"

#define GREET_MSG_FILE_MIN_SIZE				10

#define MY_TIMER_IDX						1
#define TIME_DISPLAY_ADDRESS				0x6dcbe8

#define CUR_PLAYER_PTR				(DWORD)0x853bb0

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
WNDPROC oldEditProc;

HINSTANCE g_hInst;

typedef unsigned int(__stdcall *ThreadFuncPtr)(void *);


typedef struct _func_args
{
	char *lpstr_ptr;
	size_t lpstr_size;
	char *lpstr_cmd_ptr;
	size_t lpstr_cmd_size;
	DWORD color;
	DWORD func_len;
	ThreadFuncPtr nox_func_addr;
}ARG_LIST;

typedef struct _editText
{
	HFONT hFont;
	HFONT bFont;
	HFONT hFont2;
	HFONT bigFont1;
	HWND main_wnd;
	HWND hEdit;
	HWND hMainStatic;
	DWORD lines;
	HBRUSH hBrush1;
	HBRUSH hDlgUnselBrush;
	HBRUSH hDlgSelBrush;

	HBRUSH mStaticBrush;
	int miniWndX;
	int miniWndY;
	HWND miniWnd;
	unsigned char minWndClick;
	HANDLE hTimer; //added

	HBRUSH selectbrush;
	HBRUSH hotbrush;
	HBRUSH defaultbrush;
	HBRUSH push_hotbrush1;
	HBRUSH push_hotbrush2;
	HBRUSH push_checkedbrush;
	HBRUSH push_uncheckedbrush;

	TCHAR content[MAX_EDIT_CONTENTS];
}EDIT_TXT;

typedef struct _gameset
{
	unsigned char warShield;
	unsigned char xbowDelay;
	unsigned char showChatBoard;
	unsigned char hp_bar;
	unsigned char hide_wnd;
	unsigned char key_share;
	unsigned char hookMapScr;
	unsigned char noLimit;
	unsigned char noLimitNom;
	unsigned char notifUse;
	unsigned char enabledportfwd;
	char cmdLine[20];
	char userNotif[200];
}GAME_FLAGS;

typedef struct _processMask
{
	HWND hwnd;
	HANDLE pground;
	HANDLE noxPthread;
	HANDLE subThread;
	DWORD process_id;
	DWORD subThdId;
	DWORD chat_slot;
	unsigned char threadExit;
	unsigned char noxThrdEnd;
	void *userLadder;
	int lchVer;
	int servUser;
	int servGame;
	clock_t sTime;
	unsigned char nox_exec;
	unsigned char load_nox_path;
	unsigned char enable_exec;
	unsigned char already_exist;
	unsigned char start_chat;
	unsigned char chat_timer;
	char nox_exe_name[NOX_EXE_NAME_LEN];
	char nox_path[NOX_PATH_LEN];
	char map_name_buff[NOX_EXE_NAME_LEN];
	char closePass;
	char patchGet[100];
	char **appArgv;
	wchar_t cur_path[NOX_PATH_LEN];
	EDIT_TXT *edit_ptr;
	GAME_FLAGS *flag_ptr;
	ARG_LIST *arglist_ptr;
} PMASK;

void *PROC_MASK[2];

#endif