
#include "ladderSys.h"
#include "tools.h"
#include "ladderFile.h"
#include "nox_func.h"


void ResetLadderData(LadderClass *ladder)
{
	ladder->allDeath = 0;
	ladder->allKills = 0;
}

void UnusedFieldZeroSet(LadderClass *ladder)
{
	ladder->allGens = 0;
	ladder->allMons = 0;
	ladder->allSecret = 0;
	ladder->allQDeath = 0;
	ladder->allGameCount = 0;
	ladder->allVictory = 0;
	ladder->curQDeath = 0;
	ladder->curGame = 0;
	ladder->allDefeat = 0;
}

void LadderGetOffset(LadderClass *ladder)
{
	ladder->killOffset[1] = NOX_SERVER_KILL;
	ladder->killOffset[0] = NOX_CLIENT_KILL;
	ladder->deathOffset[1] = NOX_SERVER_DEATH;
	ladder->deathOffset[0] = NOX_CLIENT_DEATH;

	ladder->getLevel = 0;
}

void GetQuestModeFlag(PMASK *mem, char *flag)
{
	int offset = NOX_CGAME_FLAG;

	if (GetMemory(mem, offset))
		flag[0] = (char)GetMemory(mem, offset);
	else if (GetMemory(mem, offset + 4))
		flag[0] = (char)GetMemory(mem, offset + 4);
	else
		flag[0] = 0;
}

void GameLoopOnJoinToServer(PMASK *pmask)
{
	LadderClass *ladder = (LadderClass *)malloc(sizeof(LadderClass));

	ladder->curDeath = 0;
	ladder->curKills = 0;
	ladder->mainProc = pmask;
	ladder->isServer = (IsHostPlayer(pmask) > 0);
	GetQuestModeFlag(pmask, &ladder->isQuestMode);
	UnusedFieldZeroSet(ladder);
	LadderGetOffset(ladder);
	if (pmask->userLadder != NULL)
		free(pmask->userLadder);
	/*(LadderClass *)*/pmask->userLadder = ladder;
	ReadMemString(pmask, 0x656e78, ladder->userId, 12);
	ladder->ladderEnable = ladder->userId[0];
	while (1)
	{
		if (ladder->ladderEnable)
		{
			if (LoadLadderData(ladder))
				break;
		}
		ResetLadderData(ladder);
		break;
	}
}

void GameLoopOnExitToServer(PMASK *pmask)
{
	LadderClass *ladder = (LadderClass *)pmask->userLadder;

	if (ladder != NULL)
	{
		if (ladder->ladderEnable)
		{
			SaveLadderData(ladder);
		}
		free(pmask->userLadder);
		pmask->userLadder = NULL;
	}
}

void LadderCalc(PMASK *mainProc, int addr, int *cur, int *total)
{
	int val = GetMemory(mainProc, addr);

	if (val ^ (*cur))
	{
		if (!(val >> 0xc))
		{
			if (val & 0xfff)
				*total = (*total) + (val - (*cur));
			*cur = val;
		}
	}
}

int IsMemWordValue(PMASK *mem, int addr)
{
	if (GetMemoryWord(mem, addr))
	{
		SetMemoryWord(mem, addr, 0);
		return 1;
	}
	return 0;
}

int GetGameResult(PMASK *mem)
{
	int res = 0;

	if (IsMemWordValue(mem, 0x58d81d)) //Victory
		res ^= 1;
	if (IsMemWordValue(mem, 0x58d7f1)) //Defeat
		res ^= 2;
	return res;
}

int LadderCalcGameCount(LadderClass *ladder, PMASK *mem, char isNotObserver)
{
	int val = (int)GetMemoryWord(mem, 0x58d875);
	int *cur = &ladder->curGame;
	int gameResult = 0;

	if (val ^ (*cur))
	{
		if (!(val >> 0xc))
		{
			gameResult = GetGameResult(mem);
			if (val & 0xfff && isNotObserver)
			{
				if (val > *cur)
				{
					ladder->allGameCount += (val - (*cur));
					if (gameResult & 1)
						ladder->allVictory++;
					else if (gameResult & 2)
						ladder->allDefeat++;
				}
			}
			else if (gameResult)
				gameResult = 0;
			*cur = val;
		}
	}
	return gameResult;
}

//0069ccc4

float CalcWinRate(int death, int kill)
{
	float de = (float)death, ki = (float)kill;

	return (float)(ki / (de + ki) * 100.0);
}

void ShowWinRate(LadderClass *ladder, char *msg)
{
	char rateMsg[20] = { 0, };
	int msgLen = StrManGetLen(msg);
	int de = ladder->allDeath, ki = ladder->allKills;

	if ((de + ki) > 40)
	{
		sprintf_s(rateMsg, sizeof(rateMsg), ", 승률: %.01f", CalcWinRate(de, ki));
		StrManCopyString(rateMsg, strlen(rateMsg), msg + msgLen);
	}
}

void ShowLadderStat(LadderClass *ladder, char *dest, int destLen)
{
	char userId[20] = { 0, };

	StrManCopyString(ladder->userId, StrManGetLen(ladder->userId), userId);
	if (ladder->isQuestMode)
		sprintf_s(dest, (size_t)destLen, "say %s 님의 퀘스트전적\n몬스터:%d킬, 오벨리스크: %d킬, 비밀구역: %d개, %d데스", 
			userId, ladder->allMons, ladder->allGens, ladder->allSecret, ladder->allQDeath);
	else
	{
		sprintf_s(dest, (size_t)destLen, "say %s 님의 게임전적: %d킬, %d데스, %d게임(승: %d, 패: %d)", 
			userId, ladder->allKills, ladder->allDeath, ladder->allGameCount, ladder->allVictory, ladder->allDefeat);
		ShowWinRate(ladder, dest);
	}
}

void PrintLadderStat(LadderClass *ladder, int say)
{
	char info[400] = { 0, };

	ShowLadderStat(ladder, info, sizeof(info));
	NoxConsolePrintMessage(ladder->mainProc, 10, info + 4);
	if (say)
		NoxCmdOutput(ladder->mainProc, 1, info);
}

void LadderTypingOnConsole(LadderClass *ladder)
{
	int offset = 0x69c9c8;
	PMASK *mainProc = ladder->mainProc;

	if (!GetMemory(mainProc, offset + 0x2fc))
	{
		SetMemory(mainProc, offset + 0x2fc, 1);
		if (!(GetMemory(mainProc, offset) ^ 0xc801c804))
			PrintLadderStat(ladder, !GetMemory(mainProc, offset + 4));
	}
}

int FindCurrentUserSection(PMASK *mem, int cUserPtr, int offset)
{
	for (int i = 0; i < 6; i++)
	{
		if (!(GetMemory(mem, offset) ^ cUserPtr))
			return offset;
		offset += 16;
	}
	return 0;
}

void LoadQuestScore(LadderClass *ladder, int cUserPtr)
{
	int offset = FindCurrentUserSection(ladder->mainProc, cUserPtr, 0x69f900);
	int mons;

	if (offset)
	{
		mons = GetMemory(ladder->mainProc, offset + 4);
		ladder->allMons += mons & 0xffff;
		ladder->allGens += mons >> 0x10;
		ladder->allSecret += (GetMemory(ladder->mainProc, offset + 8) & 0xff);
	}
}

int QuestDeathCount(PMASK *mem, int *tt)
{
	int alive = (GetMemory(mem, 0x6df344) > 0);

	if (alive ^ (*tt))
	{
		*tt = alive;
		return !alive;
	}
	return 0;
}

void QuestGameScore(LadderClass *ladder)
{
	PMASK *mem = ladder->mainProc;
	int *mLv = &ladder->getLevel;
	int curLv = GetMemory(mem, 0x69f490);

	if (*mLv ^ curLv)
	{
		if (curLv > 20)
		{
			LoadQuestScore(ladder, GetMemory(mem, CUR_PLAYER_PTR));
		}
		*mLv = curLv;
	}
}

int ClientIsNotObserve(PMASK *mem)
{
	return GetMemory(mem, 0x59abbc);
}

void GameLoopOnGame(LadderClass *ladder)
{
	PMASK *mem = ladder->mainProc;
	unsigned char serv;

	serv = ladder->isServer;
	if (ladder->isQuestMode)
	{
		QuestGameScore(ladder);
		if (QuestDeathCount(mem, &ladder->curQDeath))
			ladder->allQDeath++;
	}
	else
	{
		LadderCalc(mem, ladder->killOffset[serv], &ladder->curKills, &ladder->allKills);
		LadderCalc(mem, ladder->deathOffset[serv], &ladder->curDeath, &ladder->allDeath);
		//LadderCalcWord(mem, 0x58d875, &ladder->curGame, &ladder->allGameCount, ClientIsNotObserve(mem));
		if (LadderCalcGameCount(ladder, mem, ClientIsNotObserve(mem)) & 1)
			PrintLadderStat(ladder, 1);
	}
	LadderTypingOnConsole(ladder);
}