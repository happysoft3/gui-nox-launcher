
#include "head.h"
#include "chat_fix.h"
#include "nox_func.h"
#include "map_scr.h"
#include "tools.h"
#include "nox_proc.h"
#include "ladderSys.h"

static char s_pIndex = 0;


void ServerWebsiteButtonHandlerInit(PMASK *pmask)
{
	char myFUrl[52] = { 0, }, mySUrl[72] = { 0, };

	sprintf_s(myFUrl, sizeof(myFUrl), "http://xwis.net/nox/online");
	sprintf_s(mySUrl, sizeof(mySUrl), "https://cafe.naver.com/noxfriends");
	WriteMemString(pmask, 0x5a2398, myFUrl, sizeof(myFUrl));
	WriteMemString(pmask, 0x5a23d0, mySUrl, sizeof(mySUrl));
}

void MemDebugClassInit(MemDebugClass **mDebug)
{
	MemDebugClass *dbug = (MemDebugClass *)MyMemAlloc(sizeof(MemDebugClass));

	dbug->mSize = 800;
	*mDebug = dbug;
}

void MemDebugClassCheckStreamOutRange(MemDebugClass *mDebug, int curPtr)
{
	char out[100] = { 0, };

	if (curPtr >= mDebug->lastOffset)
	{
		sprintf_s(out, sizeof(out), "DebugLine: allocation over the stream! base=0x%x, end=0x%x, cur=0x%x",
			mDebug->offset, mDebug->lastOffset, curPtr);
		WriteStreamOnFile("debugError.txt", out, StrManGetLen(out));
	}
}

void* GetMemProcClass()
{
	return PROC_MASK[0];
}

void WriteCodeOnVirtSection(int *allocPtr, char *code, int codeLen)
{
	PMASK *mem = (PMASK *)GetMemProcClass();

	WriteMemString(mem, *allocPtr, code, codeLen);
	*allocPtr = *allocPtr + codeLen + 1;
}

void InsertXBowNoDelay(PMASK *pmask, int *allocPtr, char enableFlag)
{
	BYTE codes[] = {
		0x56, 0x50, 0x8B, 0x74, 0x24, 0x0C, 0x8B, 0x46, 0x08, 0x83, 0xE0, 
		0x04, 0x74, 0x12, 0x8B, 0x44, 0x24, 0x10, 0x68, 0x20, 0xA0, 0x4F, 
		0x00, 0x50, 0x56, 0xFF, 0x54, 0x24, 0x08, 0x83, 0xC4, 0x0C, 0x58, 
		0x5E, 0xC3, 0x90
	};
	BYTE runOffset[] = {
		0x00, 0x00, 0xE8, 0x39, 0x7A, 0xFC, 0xFF, 0x83, 0xC4, 0x10, 0x68, 
		0x00, 0x00, 0xFD, 0x0B, 0x6A, 0x0D, 0x56, 0xFF, 0x54, 0x24, 0x08, 
		0x83, 0xC4, 0x0C, 0x5E, 0x5D, 0x5B, 0x83, 0xC4, 0x18, 0xC3
	};
	int target = *allocPtr;

	WriteCodeOnVirtSection(allocPtr, codes, sizeof(codes));
	if (enableFlag)
	{
		WriteMemString(pmask, 0x539f20, runOffset, sizeof(runOffset));
		SetMemory(pmask, 0x539f2b, target);
	}
}

void InsertXHammerNoDelay(PMASK *pmask, int *allocPtr, char enableFlag)
{
	if (!enableFlag)
		return;

	BYTE opcodes[] = {
		0x68, 0x60, 0x19, 0x50, 0x00, 0x6A, 0x00, 0x6A, 0x00, 0x56, 0x68, 0x72, 0x03, 0x00, 0x00, 0xFF, 0x54, 0x24, 0x10, 0x83, 0xC4, 0x20, 
		0x50, 0x8B, 0xC6, 0x85, 0xC0, 0x74, 0x17, 0x8B, 0x40, 0x08, 0x83, 0xE0, 0x04, 0x74, 0x0F, 0x68, 0x20, 0xA0, 0x4F, 0x00, 0x6A, 0x0D, 
		0x56, 0xFF, 0x54, 0x24, 0x08, 0x83, 0xC4, 0x0C, 0x58, 0x68, 0x95, 0x97, 0x53, 0x00, 0xC3, 0x90
	};
	BYTE runCodes[] = {
		0x68, 0x00, 0x10, 0x75, 0x00, 0xC3, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90
	};
	int target = *allocPtr;
	int patchOffset = 0x539783;
	int *ttt = (int *)(&runCodes[1]);
	*ttt = target;

	WriteCodeOnVirtSection(allocPtr, opcodes, sizeof(opcodes));
	WriteMemString(pmask, patchOffset, runCodes, sizeof(runCodes));
}

int CalcCallNode(int curAddr, int targetAddr)
{
	return targetAddr - 5 - curAddr;
}

void InsertKorChatModification(PMASK *pMask, int *allocPtr)
{
    BYTE opcodes[] = { 
        0xFF, 0x74, 0x24, 0x04, 0xE8, 0x84, 0x58, 0xE1, 0xFF, 0x59, 0x55, 
        0x8B, 0xA9, 0x00, 0x02, 0x00, 0x00, 0x85, 0xED, 0x89, 0x2C, 0x41, 0x74, 0x01, 0x40, 0x5D, 0xC3, 0x90 };

    int *call = (int *)(opcodes + 5);

    *call = CalcCallNode(allocPtr[0] + 4, 0x56688D);
    SetMemory(pMask, 0x46a4dc, CalcCallNode(0x46a4dc - 1, allocPtr[0]));
    WriteCodeOnVirtSection(allocPtr, opcodes, sizeof(opcodes));
}

void GetGameCountFunction(PMASK *pmask, int *allocPtr)
{
	char code[] = { 0x50, 0xB8, 0x75, 0xd8, 0x58, 0x00, 0x66, 0xFF, 0x00, 0x58, 0x68, 0x90, 0x42, 0x49, 0x00, 0xC3 };
	int target = *allocPtr;

	WriteCodeOnVirtSection(allocPtr, code, sizeof(code));
	SetMemory(pmask, 0x491673 + 1, CalcCallNode(0x491673, target));
}

void AdvanceGetGameCountFunction(PMASK *pmask, int *allocPtr)
{
	char op[] = { 0x50, 0xB8, 0x75, 0xD8, 0x58, 0x00, 0x66, 0xFF, 0x00, 0x8A, 0x45, 0x00, 0x3C, 0x58, 0x75, 
		0x20, 0x66, 0x8B, 0x45, 0x01, 0x57, 0x8B, 0x3D, 0x9C, 0x31, 0x85, 0x00, 0x66, 0x39, 0xF8, 0x5F, 0x75,
		0x07, 0xB8, 0x1D, 0xD8, 0x58, 0x00, 0xEB, 0x05, 0xB8, 0xF1, 0xD7, 0x58, 0x00, 0x66, 0xFF, 0x00, 0x58, 
		0x83, 0xC5, 0x08, 0x68, 0x90, 0x42, 0x49, 0x00, 0xC3 };
	char runOp[] = {0x68, 0xac, 0xac, 0xac, 0xac, 0xc3};
	char **wrPtr = (char **)(runOp + 1);
	int target = *allocPtr;

	WriteCodeOnVirtSection(allocPtr, op, sizeof(op));
	*wrPtr = (char *)target;
	WriteMemString(pmask, 0x491670, runOp, sizeof(runOp));
}

void FastMapdownload(PMASK *pmask, int *allocPtr, char accel)
{
	char code[] = { 0x52, 0xE8, 0xAF, 0x80, 0x9A, 0xFD, 0x50, 0x51, 0x31, 0xC9, 0x80, 0xC1, 0x02, 0x8B, 0x44, 0x24, 
		0x08, 0x52, 0x31, 0xD2, 0x8A, 0x50, 0x02, 0x84, 0xD2, 0x74, 0x11, 0x84, 0xC9, 0x74, 0x0D, 0xFE, 0xC9, 0x51, 
		0x50, 0xE8, 0x8D, 0x80, 0x9A, 0xFD, 0x58, 0x59, 0xEB, 0xE8, 0x5A, 0x59, 0x58, 0x68, 0xD5, 0x99, 0x51, 0x00, 0xC3 };
	char runC[] = { 0x68, 0x00, 0x10, 0x75, 0x00, 0xC3 };
	int target = *allocPtr;

	code[0x0c] = accel & 0x0f;
	StrManWriteNumber(code + 2, CalcCallNode(target + 1, 0x5199f0));
	StrManWriteNumber(code + 0x24, CalcCallNode(target + 0x23, 0x5199f0));
	StrManWriteNumber(runC + 1, target);
	WriteCodeOnVirtSection(allocPtr, code, sizeof(code));
	WriteMemString(pmask, 0x5199cf, runC, sizeof(runC));
}

void NoKeepHere(PMASK *pmask, int *targetPtr)
{
	char code[] = { 0xA1, 0x8C, 0xFB, 0x69, 0x00, 0x83, 0xE0, 0x0F, 0x8D, 0x04, 0x00, 0x31, 0xd2, 0x68, 0x5F, 0x41, 0x44, 0x00, 0xC3 };
	char rStream[] = { 0x68, 0x00, 0x10, 0x75, 0x00, 0xC3, 0x90 };
	int target = *targetPtr;

	WriteCodeOnVirtSection(targetPtr, code, sizeof(code));
	StrManWriteNumber(rStream + 1, target);
	WriteMemString(pmask, 0x444158, rStream, sizeof(rStream));
	SetMemory(pmask, 0x69fb8c, 0x2);
}

void InsertWeaponChangeFunction(PMASK *pmask, int *allocPtr, char enableFlag)
{
	BYTE codes[] = {
		0x68, 0xF0, 0x2F, 0x4F, 0x00, 0x57, 0x56, 0xFF, 0x54, 0x24, 0x08, 0x83, 0xC4, 0x0C, 0x50, 0x56, 0x8B, 0x87, 0xEC, 
		0x02, 0x00, 0x00, 0x8B, 0x40, 0x68, 0x85, 0xC0, 0x74, 0x6A, 0x8B, 0x70, 0x08, 0x81, 0xE6, 0x00, 0x00, 0x00, 0x01, 
		0x85, 0xF6, 0x74, 0x5D, 0x8B, 0x40, 0x0C, 0x25, 0xC0, 0x1B, 0x00, 0x00, 0x85, 0xC0, 0x74, 0x51, 0x8B, 0xB7, 0xEC, 
		0x02, 0x00, 0x00, 0x8B, 0xB6, 0x14, 0x01, 0x00, 0x00, 0x8B, 0x36, 0xC1, 0xEE, 0x18, 0x85, 0xF6, 0x75, 0x3C, 0x8B, 
		0xB7, 0xF8, 0x01, 0x00, 0x00, 0x85, 0xF6, 0x74, 0x32, 0x8B, 0x46, 0x08, 0x25, 0x00, 0x00, 0x00, 0x02, 0x85, 0xC0, 
		0x74, 0x0C, 0x8B, 0x46, 0x0C, 0x83, 0xE0, 0x02, 0x85, 0xC0, 0x74, 0x02, 0xEB, 0x08, 0x8B, 0xB6, 0xF0, 0x01, 0x00, 
		0x00, 0xEB, 0xDC, 0x68, 0x50, 0xE6, 0x53, 0x00, 0x6A, 0x01, 0x6A, 0x01, 0x56, 0x57, 0xFF, 0x54, 0x24, 0x10, 0x83, 
		0xC4, 0x14, 0x5E, 0x58, 0x68, 0x5A, 0xA6, 0x53, 0x00, 0xC3, 0x90
	};
	BYTE runOffset[] = {
		0x68, 0x80, 0x10, 0x75, 0x00, 0xC3, 0x90, 0x83
	};
	int target = *allocPtr;

	WriteCodeOnVirtSection(allocPtr, codes, sizeof(codes));
	if (enableFlag)
	{
		StrManWriteNumber(runOffset + 1, target);
		WriteMemString(pmask, 0x53a650, runOffset, sizeof(runOffset));
	}
}

void InsertAdvanceKorChat(PMASK *pmask, int *allocPtr)
{
	BYTE codes[] = {
		0x8D, 0x70, 0x02, 0x66, 0x89, 0x10, 0x50, 0x56, 0x55, 0x8B, 0x35, 0x1C,
		0x7B, 0x6F, 0x00, 0x85, 0xF6, 0x74, 0x25, 0x8B, 0x76, 0x20, 0x81, 0xC6,
		0x00, 0x02, 0x00, 0x00, 0x39, 0xF0, 0x75, 0x18, 0x8B, 0x28, 0x8D, 0xB0,
		0x00, 0xFE, 0xFF, 0xFF, 0x8B, 0x80, 0x1C, 0x02, 0x00, 0x00, 0x25, 0xFF,
		0xFF, 0x00, 0x00, 0x01, 0xC0, 0x89, 0x2C, 0x30, 0x5D, 0x5E, 0x58, 0x68,
		0xB5, 0x5C, 0x56, 0x00, 0xC3, 0x90, 0x90, 0x90
	};
	int target = *allocPtr;

	WriteCodeOnVirtSection(allocPtr, codes, sizeof(codes));
	SetMemory(pmask, 0x565cac, 0x68118b66);
	SetMemory(pmask, 0x565cb0, target);
	SetMemory(pmask, 0x565cb4, 0x664141c3);
}

void InsertFixInventory(PMASK *pmask, int *allocPtr)
{
	BYTE opcode[] = {
		0x8A, 0x0C, 0x85, 0xC4, 0x4B, 0x6D, 0x00, 0x84, 0xC9, 0x74, 0x06, 0x68, 0xF8, 0x17, 0x46, 0x00, 0xC3,
		0x51, 0x56, 0x8B, 0x35, 0x8C, 0x4A, 0x6D, 0x00, 0x85, 0xF6, 0x74, 0x34, 0x8D, 0x34, 0x85, 0x38, 0x4B,
		0x6D, 0x00, 0x50, 0x52, 0xA1, 0x58, 0x4A, 0x6D, 0x00, 0x66, 0xB9, 0x24, 0x0C, 0x66, 0xF7, 0xE1, 0x50,
		0xA1, 0x5C, 0x4A, 0x6D, 0x00, 0x66, 0xB9, 0x94, 0x00, 0x66, 0xF7, 0xE1, 0x59, 0x5A, 0x8D, 0x8C, 0x08,
		0x38, 0x4B, 0x6D, 0x00, 0x58, 0x39, 0xCE, 0x75, 0x04, 0x5E, 0x59, 0xEB, 0xBA, 0x5E, 0x59, 0x68, 0x0E,
		0x18, 0x46, 0x00, 0xC3, 0x90
	};
	int target = *allocPtr;

	WriteCodeOnVirtSection(allocPtr, opcode, sizeof(opcode));
	SetMemory(pmask, 0x4617ec, 0x6888 ^ ((target & 0xffff) << 0x10));
	SetMemory(pmask, 0x4617f0, 0x90c30000 ^ (target >> 0x10));
	/*@brief
	*006d7c04 --인벤토리 현재 커서 위치(50단위로 증감되며, 인벤토리 상단 일 경우 0 임)
	*--> 무기, 갑옷 집을 때 바꾸는 부분은 00467c05 함수임 여기를 nop 처리하면 됨
	*/
	SetMemory(pmask, 0x467c05, 0x90909090);
	SetMemory(pmask, 0x467c09, 0x9090c390);
}

void DrawChatSlot(PMASK *pmask)
{
	int ptr;

	if (pmask->flag_ptr->showChatBoard)
	{
		ptr = GetMemory(pmask, 0x6d8530);
		if (ptr)
			SetMemory(pmask, ptr + 4, 0x5000);
	}
}

void BlockingSysopTrick(PMASK *pmask)
{
	//00 57 E8 CD/ FA FF FF 83
	SetMemory(pmask, 0x4441ac, 0xcde85700);
	SetMemory(pmask, 0x4441ac + 4, 0x83fffffa);
}

void InitNoxChat(PMASK *pmask)
{
	/*DWORD code_table[] = {
		0x46a630, 0x40f9810a , 0x46a634 ,0x90000001,0x46a638, 0x40b9077d , 0x46a63c , 0xeb000001,
		0x513f20, 0x0424448b,
		0x513f24, 0x587054a3,
		0x513f28, 0xdc08a300,
		0x513f2c, 0x90c30059,
		0x416e40, 0xd9e8006a,
		0x416e44, 0x83000fd0,
		0x416e48, 0x90c304c4,
		0x416ff4, 0xe8016a5f,
		0x416ff8, 0xfcf24,
		0x416ffc, 0xc304c483
	};*/
	MemDebugClass *mDebug = NULL;

	MemDebugClassInit(&mDebug);
	int sz, allocPtr = InjectNoxMemAlloc(pmask, mDebug->mSize);
	
	mDebug->offset = allocPtr;
	mDebug->lastOffset = allocPtr + mDebug->mSize;
	if (pmask->flag_ptr->noLimitNom)
		sz = 11;
	else
		sz = 4;
	//SetMemory(pmask, FIX_CHAT_ADDR, 0x200);
	//SetMemory(pmask, FIX_CHAT_ADDR2, 0x204);

	///SetMemory(pmask, 0x46a450, 0x2000c7);
	///SetMemory(pmask, 0x46a460, 0x8b000100);
	/*for (int i = 0 ; i < sz ; i ++)
		SetMemory(pmask, code_table[i*2], code_table[i*2 + 1]);*/
	//InsertAdvanceKorChat(pmask, &allocPtr);
	ServerWebsiteButtonHandlerInit(pmask);

	/*if (pmask->flag_ptr->hookMapScr)
	{
		SetMemory(pmask, 0x5388c8, 0xbc25d800);
		SetMemory(pmask, 0x5388cc, 0x890054a0);
	}*/
	InsertXBowNoDelay(pmask, &allocPtr, pmask->flag_ptr->xbowDelay);
	NoKeepHere(pmask, &allocPtr);
	AdvanceGetGameCountFunction(pmask, &allocPtr);
	//InsertXHammerNoDelay(pmask, &allocPtr, pmask->flag_ptr->xbowDelay);
    InsertKorChatModification(pmask, &allocPtr);
	InsertWeaponChangeFunction(pmask, &allocPtr, pmask->flag_ptr->warShield);
	InsertFixInventory(pmask, &allocPtr);
	FastMapdownload(pmask, &allocPtr, 1); //2 is error //why?
	MemDebugClassCheckStreamOutRange(mDebug, allocPtr);
	free(mDebug);
}

void NotifyLauncherVer(PMASK *pmask)
{
	char lchStr[100] = { 0, }, greet[100] = { 0, };
	LadderClass *ladder = pmask->userLadder;

	if (IsHostPlayer(pmask))
		SetGameFrameLimit(pmask, 1);
	else
		SetGameFrameLimit(pmask, 0);
	if (ladder == NULL || pmask->flag_ptr->notifUse)
		sprintf_s(greet, sizeof(greet), "say %s", pmask->flag_ptr->userNotif);
	else
		ShowLadderStat(ladder, greet, (int)sizeof(greet));
	sprintf_s(lchStr, sizeof(lchStr), "녹스 미니런처가 작동중입니다, v0.%d", pmask->lchVer);
	NoxConsolePrintMessage(pmask, 13, lchStr);
	NoxCmdOutput(pmask, 1, greet);
}

int CheckSpecificEdit(PMASK *pmask, int addr)
{
	int id = GetMemory(pmask, addr);

	return (!id || id == 0x776);
}

void FixChatBug(PMASK *pmask)
{
	DWORD addr = 0, flag;
	DWORD count_dwd = 0, prev = 0, ime = 0;
	WORD talk = 32, count = 0;

	ReadProcessMemory(pmask->pground, (LPCVOID)EDIT_TXT_PTR, &flag, sizeof(DWORD), NULL);
	if (flag) //입력중임
	{
		addr = GetMemory(pmask, flag + 0x20);			//TODO: 채팅열 주소얻음
		count_dwd = GetMemory(pmask, addr + 0x41c);		//TODO: 채팅중인 글자 수
		prev = GetMemory(pmask, addr) & 0xffff;			//TODO: 채팅열 맨 앞
		count = count_dwd & 0xffff;				//TODO: +0x41c 에는 하위2bytes= 글자수, 상위2bytes= IME입력여부
												//TODO: 그 중 입력된 글자 개수를 알아내기 위해 하위 bytes 만 가져감
		if (CheckIngameChatWnd(pmask, addr))
		{
			if (pmask->start_chat < 3) // && CheckIngameChatWnd(pmask, addr)) //채팅창 입력 초기화(비우기) 30
			{
				if (prev)
				{
					pmask->start_chat = 0x7f;
					//ClearNoxEditWnd(pmask, addr);
					SetMemory(pmask, addr, 0);
					SetMemory(pmask, addr + 0x41c, 0);
				}
				else
					pmask->start_chat += 1;
			}
			else if (!prev)
			{
				ClearNoxEditWnd(pmask, addr);
				SetMemory(pmask, addr + 0xec, 1);
			}
		}
		else if (CheckSpecificEdit(pmask, flag))
		{
			if (pmask->start_chat)
			{
				if (prev)
				{
					SetMemory(pmask, addr, 0);
					SetMemory(pmask, addr + 0x41c, 0);
					pmask->start_chat = 0;
				}
				else
					pmask->start_chat -= 1;
			}
			else if (!GetMemory(pmask, addr + 0xec))
			{
				pmask->start_chat = 10;
				SetMemory(pmask, addr + 0xec, 1);
			}
		}
	}
	else if (pmask->start_chat)
	{
		pmask->start_chat = 0;
	}
}

void DisplayHealthBar(PMASK *pmask)
{
	if (pmask->flag_ptr->hp_bar)
	{
		if (GetMemory(pmask, GetMemory(pmask, 0x6df368) + 4) & 0x10)
			SetMemory(pmask, GetMemory(pmask, 0x6df368) + 4, 0x08);
		if (GetMemory(pmask, 0x85e3d8) >> 0x10)
		{
			if (GetMemory(pmask, GetMemory(pmask, 0x6df37c) + 4) & 0x10)
				SetMemory(pmask, GetMemory(pmask, 0x6df37c) + 4, 0x08);
		}
	}
}

void ChatSendConsole(PMASK *pmask, char *use_cmd)
{
	char token = 0, dest[200];
	wchar_t read_cmd[100] = { 0, };

	ReadProcessMemory(pmask->pground, (LPCVOID)CHAT_COMPLETE, &token, sizeof(char), NULL);
	if (token == '/')
	{
		token = ' ';
		WriteProcessMemory(pmask->pground, (LPVOID)CHAT_COMPLETE, &token, sizeof(char), NULL);
		ReadProcessMemory(pmask->pground, (LPCVOID)CHAT_COMPLETE, read_cmd, sizeof(read_cmd), NULL);
		WideCharToMultiByte(CP_ACP, 0, read_cmd, -1, dest, 200, NULL, NULL);
		NoxConsolePrint(pmask, use_cmd, 10, strlen(use_cmd), dest, sizeof(dest));
	}
}

void CheckRevisionUser(PMASK *pmask, char pIndex)
{
    int pOffset = 0x62f9e0 + (pIndex * 0x12dc);

    if (GetMemory(pmask, pOffset))
    {
        if (!GetMemory(pmask, pOffset + 40))
            NoxServerKickClient(pmask, pIndex);
    }
}

void AlwaysKeySharing(PMASK *pmask)
{
	if (GetMemory(pmask, HOST_PLAYER_ADDR))
	{
		if (!pmask->flag_ptr->key_share) return;
		if (!GetMemory(pmask, SHARE_KEY1))
		{
			SetMemory(pmask, SHARE_KEY1, 1);
			SetMemory(pmask, SHARE_KEY2, 1);
		}

        for (int u = 0 ; u < 5 ; u ++)
        {
            CheckRevisionUser(pmask, s_pIndex);
            if (s_pIndex >= 30)
                s_pIndex = 0;
            else
                ++s_pIndex;
        }
	}
}

void HookQuestModeScript(PMASK *pmask)
{
	if (!pmask->flag_ptr->hookMapScr) return;
	if (IsHostPlayer(pmask) && CheckQuestModeFlag(pmask))
		NoxScriptMain(pmask);
}

int CheckIngameChatWnd(PMASK *pmask, DWORD ptr)
{
	return GetMemory(pmask, CHAT_ADDR) == ptr;
}

void ClearNoxEditWnd(PMASK *pmask, DWORD ptr)
{
	BYTE empty[200] = { 0, };

	empty[0] = 32;
	WriteMemString(pmask, ptr, empty, sizeof(empty));
	SetMemory(pmask, ptr + 0x41c, 1);
}