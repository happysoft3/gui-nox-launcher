
#include "portfwd.h"
#include "head.h"
#include "nox_proc.h"
#include "tools.h"
#include "nox_thread.h"


void BeforeAppLoop(PMASK *pmask, HWND hWnd)
{
	wchar_t appName[100] = { 0, };
	int version = pmask->lchVer;

	wsprintf(appName, L"녹스 미니런처-- 버전v0.%d", version);
	SetWindowText(hWnd, appName);
}

void LauncherFlagInit(PMASK *pmask)
{
	pmask->flag_ptr->hp_bar = 0;
	pmask->flag_ptr->hide_wnd = 0;
	pmask->flag_ptr->key_share = 0;
	pmask->flag_ptr->noLimit = 0;
	pmask->flag_ptr->noLimitNom = 0;
	pmask->flag_ptr->notifUse = 0;
	pmask->flag_ptr->showChatBoard = 0;
	pmask->flag_ptr->warShield = 0;
	pmask->flag_ptr->xbowDelay = 0;
	pmask->flag_ptr->hookMapScr = 0;
	pmask->flag_ptr->enabledportfwd = 0;
}

static void PortfowardCallback(void)
{
	//PMASK *pmask = PROC_MASK[0];

	//if (pmask->flag_ptr->enabledportfwd)
	//{
		//AppendText(pmask->edit_ptr, L"자동 포트포워딩이 작동하고 있습니다\r\n");
	//}
}

void InitProcessMask(PMASK *pmask, EDIT_TXT *edit_str)
{
	srand((UINT32)(time(0)));
	pmask->hwnd = NULL;
	pmask->already_exist = 0;
	pmask->chat_timer = 0;
	pmask->chat_slot = 0;
	pmask->enable_exec = 0;
	pmask->load_nox_path = 0;
	pmask->nox_exec = 0;
	pmask->start_chat = 0;
	pmask->edit_ptr = edit_str;
	pmask->edit_ptr->miniWnd = NULL;
	pmask->threadExit = 0;
	pmask->noxThrdEnd = 0;
	pmask->closePass = 0;
	memset(pmask->map_name_buff, 0, NOX_EXE_NAME_LEN);
	memset(pmask->nox_path, 0, NOX_PATH_LEN);
	memset(pmask->nox_exe_name, 0, NOX_EXE_NAME_LEN);
	memset(pmask->flag_ptr->cmdLine, 0, 20);
	PROC_MASK[0] = pmask;
	LauncherFlagInit(pmask);
	pmask->lchVer = 11398;
	pmask->servUser = 0;
	pmask->servGame = 0;
	pmask->sTime = clock();
	pmask->userLadder = NULL;
	memset(edit_str->content, 0, (size_t)1000);
	edit_str->lines = 0;
	sprintf_s(pmask->flag_ptr->userNotif, (size_t)100, "녹스 미니런처 유저입니다, 버전: v0.%d", pmask->lchVer);
	PROC_MASK[1] = edit_str;

	if (GetTargetPathFile(pmask, LAUNCHER_FILE_NAME))
		pmask->load_nox_path = 1;
	if (SearchProcessID(pmask))
	{
		pmask->already_exist = 1;
	}
	SaveApplicationDirectory(pmask);
	CreateSubThread(pmask);
	PortFwdCreateThread((int *)&pmask->flag_ptr->enabledportfwd, &PortfowardCallback);
}



int CheckNoxPathFile(HWND hWnd)
{
	FILE *fp = NULL;

	fopen_s(&fp, LAUNCHER_FILE_NAME, "rt");
	if (fp != NULL)
	{
		fclose(fp);
		return 1;
	}
	else
	{
		MessageBox(
			hWnd,
			L"아직 녹스 실행파일 경로가 지정되지 않았기 때문에 실행할 수 없습니다, '런처옵션' 버튼을 누르신 후 녹스 실행파일 경로를 지정하세요",
			L"녹스 실행파일 경로 지정이 필요합니다",
			MB_OK | MB_ICONSTOP
		);
		return 0;
	}
}

void RunNox(HWND hWnd, PMASK *pmask, EDIT_TXT *edit_str)
{
	HANDLE noxThread, noxLoopThread;
	DWORD noxThreadPid = 0, noxLoopPid = 0;
	TCHAR res_notify[40] = { 0, };

	if (pmask->nox_path[0] && !pmask->already_exist && pmask->enable_exec)
	{
		if (!pmask->nox_exec && !SearchProcessID(pmask))
		{
			pmask->nox_exec = 1;
			SetWindowText(edit_str->hEdit, L"녹스를 실행합니다.\r\n");
			//TODO: create noxExecThread
			noxThread = (HANDLE)_beginthreadex(NULL, 0, NoxExecThread, pmask, 0, (unsigned*)noxThreadPid);
			if (!noxThread)
			{
				AppendText(edit_str, L"녹스 스레드 생성에 실패했습니다.\r\n");
				pmask->nox_exec = 0;
			}
			else
			{
				pmask->noxPthread = noxThread;
				AppendText(edit_str, res_notify);
				//TODO: createThread for wait run nox
				noxLoopThread = (HANDLE)_beginthreadex(NULL, 0, NoxLoopThread, pmask, 0, (unsigned*)noxLoopPid);
			}
		}
		else
		{
			MessageBox(hWnd, L"녹스가 이미 실행중입니다", L"알림", MB_OK | MB_ICONWARNING);
		}
	}
}

void OpenNoxDirectory(PMASK *pmask)
{
	char cmd[200] = { 0, };

	if (pmask->enable_exec)
	{
		sprintf_s(cmd, sizeof(cmd), "explorer.exe %s", pmask->nox_path);
		WinExec(cmd, SW_SHOW);
	}
}

void OpenXwisUsers(PMASK *pmask)
{
	char cmd[200] = { 0, };

	if (pmask->enable_exec)
	{
		sprintf_s(cmd, sizeof(cmd), "explorer.exe http://xwis.net/nox/online");
		WinExec(cmd, SW_SHOW);
	}
}

void OpenNaverCafe(PMASK *pmask)
{
	char cmd[200] = { 0, };

	if (pmask->enable_exec)
	{
		sprintf_s(cmd, sizeof(cmd), "explorer.exe https://cafe.naver.com/noxfriends/964");
		WinExec(cmd, SW_SHOW);
	}
}

int EndProgram(PMASK *pmask, HWND hWnd)
{
	if (pmask->closePass) return 1;
	if (MessageBox(hWnd, L"녹스 미니런처를 셧 다운 합니까?\n계속하려면 '예'를 누르세요", L"프로그램 종료", MB_YESNO | MB_ICONWARNING | MB_DEFBUTTON2) == IDYES)
	{
		if (pmask->nox_exec)
			MessageBox(hWnd, L"녹스를 먼저 종료해 주신 후 미니런처를 꺼주세요", L"프로그램 종료", MB_OK | MB_ICONERROR);
		else
		{
			return 1;
		}
	}
	return 0;
}

void ThisShutdown(PMASK *pmask)
{
	if (IsWindow(pmask->edit_ptr->miniWnd))
	{
		DestroyWindow(pmask->edit_ptr->miniWnd);
		pmask->edit_ptr->miniWnd = NULL;
	}
	pmask->threadExit = 1;
	WaitForSingleObject(pmask->subThread, SUB_THREAD_TIMEOUT);
	CloseHandle(pmask->subThread);
}

void SetGameFrameLimit(PMASK *pmask, char tof)
{
	char set = tof % 2;

	if (pmask->flag_ptr->noLimitNom)
	{
		SetMemory(pmask, 0x587054, set);
		SetMemory(pmask, 0x5d4728, 4 - set);
		SetMemory(pmask, 0x5d47c4, 2 - set);
		//SetMemory(pmask, 0x83aabc, set - 1);
		SetMemory(pmask, 0x59dc08, set);
	}
}


void UnParseUserData(PMASK *pmask, int flag)
{
	if (flag & 1)
		pmask->flag_ptr->key_share = 1;
	if (flag & 2)
		pmask->flag_ptr->noLimit = 1;
	if (flag & 4)
		pmask->flag_ptr->warShield = 1;
	if (flag & 8)
		pmask->flag_ptr->hide_wnd = 1;
	if (flag & 0x10)
		pmask->flag_ptr->noLimitNom = 1;
	if (flag & 0x20)
		pmask->flag_ptr->xbowDelay = 1;
	if (flag & 0x40)
		pmask->flag_ptr->hp_bar = 1;
	if (flag & 0x80)
		pmask->flag_ptr->showChatBoard = 1;
	if (flag & 0x100)
		pmask->flag_ptr->notifUse = 1;
	if (flag & 0x200)
		pmask->flag_ptr->hookMapScr = 1;
}

void GetPreviousUserSetting(PMASK *pmask, char *fn)
{
	FILE *fp = NULL;
	int setFileSize, read = 0;

	fopen_s(&fp, fn, "rb");
	if (fp != NULL)
	{
		setFileSize = GetUserDataFileSize(fp);
		if (setFileSize >= sizeof(int)) //TODO: at least 4bytes
		{
			fread_s(&read, sizeof(int), sizeof(int), (size_t)1, fp);
			UnParseUserData(pmask, read);
			if (setFileSize >= 14)
				GetUserNotifyMessage(pmask, fp);
			AppendText(pmask->edit_ptr, L"이미 세팅된 옵션이 로드되었습니다\r\n");
		}
		fclose(fp);
	}
}