
#include "NoxCryptApi.h"
#include "NoxCryptKey.h"


void *NoxAPICrypt(unsigned char *data, NoxFormat format, int dataLength, int mode)
{
	int tableOffset;
	int part1, part2;
	const unsigned long *table = NULL;
	unsigned char *output = (char *)malloc(sizeof(char) * dataLength + 1);

	memset(output, 0, dataLength + 1);
	switch (format)
	{
	case NOX_SAVE:
		table = SAVE_TABLE;
		break;
	case NOX_MAP:
		table = MAP_TABLE;
		break;
	case NOX_MODIFIER:
		table = MOD_TABLE;
		break;
	case NOX_MONSTER:
		table = MONSTER_TABLE;
		break;
	case NOX_SOUNDSET:
		table = SOUND_TABLE;
		break;
	case NOX_THING:
		table = THING_TABLE;
		break;
	case NOX_GAMEDATA:
		table = GAMEDATA_TABLE;
		break;
	default:
		sprintf_s(output, 6, "error");
		return output;
	}
	if (mode) //TODO: Encrypt
		tableOffset = 0x400;
	else //TODO: Decrypt
		tableOffset = 0x418;
	for (int dataNdx = 0; dataNdx < dataLength; dataNdx += 8)
	{
		int offset = tableOffset;

		part1 = (data[dataNdx + 0] << 24) | (data[dataNdx + 1] << 16) | (data[dataNdx + 2] << 8) | data[dataNdx + 3];
		part2 = (data[dataNdx + 4] << 24) | (data[dataNdx + 5] << 16) | (data[dataNdx + 6] << 8) | data[dataNdx + 7];

		for (int i = 0; i < 8; i++)
		{
			unsigned int sum = 0;
			part1 ^= table[offset++];
			sum += table[0x000 + ((part1 >> 24) & 0xFF)];
			sum += table[0x100 + ((part1 >> 16) & 0xFF)];
			sum ^= table[0x200 + ((part1 >> 8) & 0xFF)];
			sum += table[0x300 + ((part1 >> 0) & 0xFF)];

			sum ^= table[offset++];
			part2 ^= sum;

			sum = 0;
			sum += table[0x000 + ((part2 >> 24) & 0xFF)];
			sum += table[0x100 + ((part2 >> 16) & 0xFF)];
			sum ^= table[0x200 + ((part2 >> 8) & 0xFF)];
			sum += table[0x300 + ((part2 >> 0) & 0xFF)];
			part1 ^= sum;
		}

		part1 ^= table[offset++];
		part2 ^= table[offset];

		output[dataNdx + 0] = (unsigned char)((part2 >> 24) & 0xFF);
		output[dataNdx + 1] = (unsigned char)((part2 >> 16) & 0xFF);
		output[dataNdx + 2] = (unsigned char)((part2 >> 8) & 0xFF);
		output[dataNdx + 3] = (unsigned char)((part2 >> 0) & 0xFF);
		output[dataNdx + 4] = (unsigned char)((part1 >> 24) & 0xFF);
		output[dataNdx + 5] = (unsigned char)((part1 >> 16) & 0xFF);
		output[dataNdx + 6] = (unsigned char)((part1 >> 8) & 0xFF);
		output[dataNdx + 7] = (unsigned char)((part1 >> 0) & 0xFF);
	}
	return output;
}