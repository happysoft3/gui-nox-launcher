
#include "head.h"
#include "tools.h"
#include "nox_proc.h"
#include "setGui.h"

#define GUI_COLOR_BASE_PANEL RGB(70, 156, 198)
#define GUI_COLOR_TEXTINPUT_BASE RGB(153, 217, 234)
#define GUI_COLOR_TEXTINPUT_TEXT RGB(28, 71, 91)

void SetMyButtonPaint(EDIT_TXT *mWndPtr)
{
	mWndPtr->defaultbrush = NULL;
	mWndPtr->hotbrush = NULL;
	mWndPtr->selectbrush = NULL;
	mWndPtr->push_uncheckedbrush = NULL;
	mWndPtr->push_checkedbrush = NULL;
	mWndPtr->push_hotbrush1 = NULL;
	mWndPtr->push_hotbrush2 = NULL;
}

LRESULT CALLBACK WndCtrlTextLockFieldLock(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_PASTE: case WM_COPY:
	case WM_RBUTTONDOWN: case WM_LBUTTONDOWN:
		return 0;
	}
	return CallWindowProcA(oldEditProc, hWnd, msg, wParam, lParam);
}

void StartupMessage(HWND hWnd, PMASK *pmask, EDIT_TXT *edit_str)
{
	HWND butn[6];

	edit_str->bFont = CreateFont(15, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1,
		VARIABLE_PITCH | FF_ROMAN, L"맑은 고딕");
	edit_str->bigFont1 = CreateFont(38, 0, 0, 0, 200, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1,
		VARIABLE_PITCH | FF_ROMAN, L"맑은 고딕");
	edit_str->hFont = CreateFont(14, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1,
		VARIABLE_PITCH | FF_ROMAN, L"MS Shell Dlg");
	edit_str->hFont2 = CreateFont(13, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, L"휴먼고딕");
	edit_str->hBrush1 = CreateSolidBrush(GUI_COLOR_TEXTINPUT_BASE);

	butn[0] = CreateWindow(WC_BUTTON, L"녹스 실행", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, 25, 130, 310, 53, hWnd, (HMENU)0, g_hInst, NULL);
	butn[1] = CreateWindow(WC_BUTTON, L"런처옵션", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 355, 23, 100, 25, hWnd, (HMENU)1, g_hInst, NULL);
	butn[2] = CreateWindow(WC_BUTTON, L"녹스 폴더열기", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 355, 58, 100, 25, hWnd, (HMENU)2, g_hInst, NULL);
	butn[3] = CreateWindow(WC_BUTTON, L"xwis서버 현황", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 355, 93, 100, 25, hWnd, (HMENU)3, g_hInst, NULL);
	butn[4] = CreateWindow(WC_BUTTON, L"서버 X-On", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 355, 128, 100, 25, hWnd, (HMENU)4, g_hInst, NULL);
	butn[5] = CreateWindow(WC_BUTTON, L"패치로그 확인", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 355, 163, 100, 25, hWnd, (HMENU)6, g_hInst, NULL);
	edit_str->hEdit = CreateWindow(WC_EDIT, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_READONLY | ES_MULTILINE,
		20, 20, 320, 100, hWnd, (HMENU)5, g_hInst, NULL);
	edit_str->hMainStatic = CreateWindow(WC_STATIC, L"", WS_CHILD | WS_VISIBLE | SS_CENTER,
		20, 200, 280, 25, hWnd, (HMENU)-1, g_hInst, NULL);

	edit_str->mStaticBrush = CreateSolidBrush(GUI_COLOR_BASE_PANEL);
	SendMessage(edit_str->hEdit, WM_SETFONT, (WPARAM)edit_str->hFont, 1);
	SendMessage(butn[0], WM_SETFONT, (WPARAM)edit_str->bigFont1, 1);
	SendMessage(butn[1], WM_SETFONT, (WPARAM)edit_str->bFont, 1);
	SendMessage(butn[2], WM_SETFONT, (WPARAM)edit_str->bFont, 1);
	SendMessage(butn[3], WM_SETFONT, (WPARAM)edit_str->bFont, 1);
	SendMessage(butn[4], WM_SETFONT, (WPARAM)edit_str->bFont, 1);
	SendMessage(butn[5], WM_SETFONT, (WPARAM)edit_str->bFont, 1);
	SendMessage(edit_str->hMainStatic, WM_SETFONT, (WPARAM)edit_str->hFont, 1);

	//UI 생성 끝_ 아래는 녹스 실행 체크부분(중복실행 방지)
	if (pmask->already_exist)
		AppendText(edit_str, L"이미 녹스가 런처없이 실행중입니다\r\n");
	else if (CheckNoxExecFile(pmask))
	{
		SetWindowText(edit_str->hEdit, L"녹스 실행파일 경로를 로드했습니다\r\n");
		pmask->enable_exec = 1;
	}
	GetPreviousUserSetting(pmask, USER_SETTING_FILE_NAME);
	if (pmask->flag_ptr->enabledportfwd)
	{
		AppendText(pmask->edit_ptr, L"자동 포트포워딩이 작동하고 있습니다\r\n");
	}
	pmask->edit_ptr->hTimer = NULL;
	CreateMyTimer(pmask, hWnd);
	SetMyButtonPaint(edit_str);
	oldEditProc = (WNDPROC)SetWindowLong(edit_str->hEdit, GWLP_WNDPROC, (LONG)WndCtrlTextLockFieldLock);
}

void DrawNoxStatus(PMASK *pmask, HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	HFONT org_font = (HFONT)SelectObject(hdc, pmask->edit_ptr->hFont2);

	SetBkColor(hdc, GUI_COLOR_BASE_PANEL);
	if (pmask->nox_exec)
	{
		SetTextColor(hdc, RGB(16, 57, 222));
		TextOut(hdc, 360, 210, L"녹스 실행중", 6);
	}
	else
	{
		SetTextColor(hdc, RGB(163, 74, 164));
		TextOut(hdc, 360, 210, L"녹스 실행대기", 7);
	}
	SelectObject(hdc, org_font);
	EndPaint(hWnd, &ps);
}

void UpdateNoxStatTab(PMASK *pmask)
{
	RECT rt;

	//SetRect(&rt, 260, 170, 320, 210);
	SetRect(&rt, 360, 210, 420, 250);
	InvalidateRect(pmask->hwnd, &rt, 1);
}

int CheckMainWnd(PMASK *pmask, HWND hWnd)
{
	return (hWnd == pmask->edit_ptr->hEdit);
}

LRESULT StaticCustomBrush(EDIT_TXT *mWnd, HDC hdc)
{
	SetTextColor(hdc, RGB(237, 28, 36));
	SetBkColor(hdc, GUI_COLOR_BASE_PANEL);

	return (LRESULT)mWnd->mStaticBrush;
}

LRESULT SetMainEditColor(PMASK *pmask, WPARAM wParam)
{
	SetTextColor((HDC)wParam, GUI_COLOR_TEXTINPUT_TEXT);
	SetBkColor((HDC)wParam, GUI_COLOR_TEXTINPUT_BASE);
	return (LRESULT)pmask->edit_ptr->hBrush1;
}

LRESULT SetLauncherButtonColor(PMASK *pmask, WPARAM wParam)
{
	SetTextColor((HDC)wParam, RGB(255, 241, 0));
	SetBkColor((HDC)wParam, RGB(0, 0, 255));
	return (LRESULT)pmask->edit_ptr->hBrush1;
}


void CreateMyTimer(PMASK *pmask, HWND hWnd)
{
	HANDLE *myTimer = &pmask->edit_ptr->hTimer;

	if (*myTimer == NULL)
	{
		*myTimer = (HANDLE *)SetTimer(hWnd, MY_TIMER_IDX, 1000, (TIMERPROC)MyTimerProc);
	}
}

void MyWriteCurrentTime(PMASK *pmask, int target)
{
	wchar_t wTime[100] = { 0, };
	SYSTEMTIME sysTime;

	GetLocalTime(&sysTime);
	wsprintf(wTime, L"현재시간: %02d:%02d:%02d", sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
	WriteMemString(pmask, target, (char *)wTime, 100);
}

void CALLBACK MyTimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	PMASK *pmask = PROC_MASK[0];
	HANDLE *myTimer = &pmask->edit_ptr->hTimer;

	if (*myTimer != NULL)
	{
		if (GetMemory(pmask, CUR_PLAYER_PTR))
		{
			MyWriteCurrentTime(pmask, TIME_DISPLAY_ADDRESS);
		}
	}
}

void RemoveMyTimer(PMASK *pmask, HWND hWnd)
{
	EDIT_TXT *mWndPtr = pmask->edit_ptr;

	KillTimer(hWnd, MY_TIMER_IDX);

	DeleteObject(mWndPtr->defaultbrush);
	DeleteObject(mWndPtr->selectbrush);
	DeleteObject(mWndPtr->hotbrush);
	DeleteObject(mWndPtr->push_checkedbrush);
	DeleteObject(mWndPtr->push_hotbrush1);
	DeleteObject(mWndPtr->push_hotbrush2);
	DeleteObject(mWndPtr->push_uncheckedbrush);
}

HBRUSH CreateGradientBrush(COLORREF top, COLORREF bottom, LPNMCUSTOMDRAW item)
{
	HBRUSH Brush = NULL;
	HDC hdcmem = CreateCompatibleDC(item->hdc);
	HBITMAP hbitmap = CreateCompatibleBitmap(item->hdc, item->rc.right - item->rc.left, item->rc.bottom - item->rc.top);
	SelectObject(hdcmem, hbitmap);

	int r1 = GetRValue(top), r2 = GetRValue(bottom), g1 = GetGValue(top), g2 = GetGValue(bottom), b1 = GetBValue(top), b2 = GetBValue(bottom);
	for (int i = 0; i < item->rc.bottom - item->rc.top; i++)
	{
		RECT temp;
		int r, g, b;
		r = (int)(r1 + (double)(i * (r2 - r1) / item->rc.bottom - item->rc.top));
		g = (int)(g1 + (double)(i * (g2 - g1) / item->rc.bottom - item->rc.top));
		b = (int)(b1 + (double)(i * (b2 - b1) / item->rc.bottom - item->rc.top));
		Brush = CreateSolidBrush(RGB(r, g, b));
		temp.left = 0;
		temp.top = i;
		temp.right = item->rc.right - item->rc.left;
		temp.bottom = i + 1;
		FillRect(hdcmem, &temp, Brush);
		DeleteObject(Brush);
	}
	HBRUSH pattern = CreatePatternBrush(hbitmap);

	DeleteDC(hdcmem);
	DeleteObject(Brush);
	DeleteObject(hbitmap);

	return pattern;
}

int OnNotifyHandler(HWND hwnd, PMASK *mProc, LPNMHDR some_item)
{
	EDIT_TXT *mWnd = mProc->edit_ptr;

	if (some_item->code == NM_CUSTOMDRAW)
	{
		LPNMCUSTOMDRAW item = (LPNMCUSTOMDRAW)some_item;

		if (item->uItemState & CDIS_SELECTED)
		{
			//Select our color when the button is selected
			if (mWnd->selectbrush == NULL)
				mWnd->selectbrush = CreateGradientBrush(RGB(0, 180, 0), RGB(180, 255, 0), item);

			//Create pen for button border
			HPEN pen = CreatePen(PS_INSIDEFRAME, 0, RGB(0, 0, 0));

			//Select our brush into hDC
			HGDIOBJ old_pen = SelectObject(item->hdc, pen);
			HGDIOBJ old_brush = SelectObject(item->hdc, mWnd->selectbrush);

			//If you want rounded button, then use this, otherwise use FillRect().
			RoundRect(item->hdc, item->rc.left, item->rc.top, item->rc.right, item->rc.bottom, 5, 5);

			//Clean up
			SelectObject(item->hdc, old_pen);
			SelectObject(item->hdc, old_brush);
			DeleteObject(pen);

			//Now, I don't want to do anything else myself (draw text) so I use this value for return:
			return CDRF_DODEFAULT;
			//Let's say I wanted to draw text and stuff, then I would have to do it before return with
			//DrawText() or other function and return CDRF_SKIPDEFAULT
		}
		else
		{
			if (item->uItemState & CDIS_HOT) //Our mouse is over the button
			{
				//Select our color when the mouse hovers our button
				if (mWnd->hotbrush == NULL)
					mWnd->hotbrush = CreateGradientBrush(RGB(230, 255, 0), RGB(0, 245, 0), item);

				HPEN pen = CreatePen(PS_INSIDEFRAME, 0, RGB(0, 0, 0));

				HGDIOBJ old_pen = SelectObject(item->hdc, pen);
				HGDIOBJ old_brush = SelectObject(item->hdc, mWnd->hotbrush);

				RoundRect(item->hdc, item->rc.left, item->rc.top, item->rc.right, item->rc.bottom, 5, 5);

				SelectObject(item->hdc, old_pen);
				SelectObject(item->hdc, old_brush);
				DeleteObject(pen);

				return CDRF_DODEFAULT;
			}

			//Select our color when our button is doing nothing
			if (mWnd->defaultbrush == NULL)
				mWnd->defaultbrush = CreateGradientBrush(RGB(180, 255, 0), RGB(0, 180, 0), item);

			HPEN pen = CreatePen(PS_INSIDEFRAME, 0, RGB(0, 0, 0));

			HGDIOBJ old_pen = SelectObject(item->hdc, pen);
			HGDIOBJ old_brush = SelectObject(item->hdc, mWnd->defaultbrush);

			RoundRect(item->hdc, item->rc.left, item->rc.top, item->rc.right, item->rc.bottom, 5, 5);

			SelectObject(item->hdc, old_pen);
			SelectObject(item->hdc, old_brush);
			DeleteObject(pen);

			return CDRF_DODEFAULT;
		}
	}
	return CDRF_DODEFAULT;
}