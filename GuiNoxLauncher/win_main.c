
#include "head.h"
#include "nox_proc.h"

#include "resource.h"

LPCWSTR lpszClass = (LPCWSTR)L"녹스 미니런처 v0.11261       Happy soft ltd";


int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	PMASK pmask;
	EDIT_TXT edit_str;
	GAME_FLAGS game_flags = { 0, };
	HWND hWnd;
	MSG Message;
	WNDCLASS WndClass;
	HANDLE hMutex = CreateMutex(NULL, TRUE, lpszClass);
	HBRUSH hBrush = CreateSolidBrush(RGB(70, 156, 198));

	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		MessageBox(NULL, L"프로그램이 이미 실행중인 상태입니다", L"이미 실행중인 프로그램", MB_OK | MB_ICONERROR);
		return 0;
	}
	g_hInst = hInstance;
	pmask.flag_ptr = &game_flags;
	pmask.appArgv = &lpszCmdParam;
	InitProcessMask(&pmask, &edit_str);
	
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hbrBackground = hBrush;
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hIcon = LoadIcon(g_hInst, MAKEINTRESOURCE( IDI_ICON3));
	WndClass.hInstance = hInstance;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.lpszClassName = lpszClass;
	WndClass.lpszMenuName = NULL;
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&WndClass);

	hWnd = CreateWindow(lpszClass, lpszClass, WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, //WS_OVERLAPPEDWINDOW WS_VSCROLL,
		CW_USEDEFAULT, 0, 495, 270, NULL, (HMENU)NULL, hInstance, NULL);
	ShowWindow(hWnd, nCmdShow);
	pmask.edit_ptr->main_wnd = hWnd;
	BeforeAppLoop(&pmask, hWnd);
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}
	ReleaseMutex(hMutex);
	CloseHandle(hMutex);
	DeleteObject(hBrush);

	return Message.wParam;
}