

#include "portfwd.h"
#include <Windows.h>
#include <process.h>
#include <stdint.h>

int PortFwdLoadModule(PortFwdThreadData *argData)
{
	void(*run)(void) = NULL;
	HMODULE hInstDll = LoadLibrary(TEXT("portfwd.dll"));

	if (hInstDll == NULL || hInstDll == INVALID_HANDLE_VALUE)
	{
		return 0;
	}
	run = (void(*)(void))GetProcAddress(hInstDll, "startProcess");

	if (run != NULL)
	{
		*argData->result = 1;
		(*argData->fwdCb)();
		(*run)();
		return 1;
	}
	return 0;
}

unsigned WINAPI PortFwdPthread(void *arg)
{
	PortFwdThreadData *argData = (PortFwdThreadData *)arg;

	PortFwdLoadModule(argData);
	free(argData);
	return 0;
}

int PortFwdCreateThread(int *result, void(*cb)(void))
{
	PortFwdThreadData *arg = (PortFwdThreadData *)malloc(sizeof(PortFwdThreadData));

	arg->result = result;
	arg->fwdCb = cb;
	uint32_t pid = 0;
	HANDLE pThread = (HANDLE)_beginthreadex(NULL, 0, &PortFwdPthread, arg, 0, &pid);

	if (pThread != NULL)
		return 1;
	else
		return 0;
}

