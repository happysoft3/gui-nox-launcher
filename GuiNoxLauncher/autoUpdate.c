

#include "head.h"
#include "tools.h"
#include "autoUpdate.h"
#include "NoxCryptApi.h"

DWORD DownloadFromURL(HINTERNET hInternet, wchar_t *pszURL, char **ptr)
{
	char fSizeStr[20] = { 0, }, *pBuffer;
	DWORD dwSize = 0, dwRead = 20, dwCor = 0, dwTSize = 0;
	HINTERNET hURL = InternetOpenUrl(hInternet, pszURL, NULL, 0, 0, 0);
	if (hURL == NULL)
	{
		return 0;
	}
	if (!HttpQueryInfo(hURL, HTTP_QUERY_CONTENT_LENGTH, fSizeStr, &dwRead, NULL))
	{
		InternetCloseHandle(hURL);
		return 0;
	}
	dwTSize = WCharToInt(fSizeStr);
	if (dwTSize > 1000000)
	{
		InternetCloseHandle(hURL);
		return 0;
	}
	else
	{
		pBuffer = (char *)malloc(sizeof(char) * (dwTSize + 1));
		memset(pBuffer, 0, sizeof(char) * (dwTSize + 1));
		*ptr = pBuffer;
	}
	do
	{
		InternetQueryDataAvailable(hURL, &dwSize, 0, 0);
		InternetReadFile(hURL, pBuffer + dwCor, dwSize, &dwRead);
		dwCor += dwRead;
	} while (dwRead);
	InternetCloseHandle(hURL);
	return dwTSize;
}

int StartPatchFileDownload(HINTERNET hInternet, char *url, char *fn)
{
	wchar_t *wUrl;
	char *ptr = NULL;
	int size = 0;

	if (hInternet != NULL)
	{
		wUrl = CharToWChar(url, strlen(url) + 1);
		size = DownloadFromURL(hInternet, wUrl, &ptr);
		if (size)
		{
			char *decryptStream = NoxAPICrypt(ptr, NOX_MONSTER, size, 0);
			if (!StoreDownloadedFile(fn, decryptStream, size))
				size = 0;
			free(decryptStream);
		}
		free(wUrl);
		if (ptr != NULL)
			free(ptr);
	}
	return size;
}

int CheckUpdateExecFile(char *fn)
{
	FILE *fp = NULL;

	fopen_s(&fp, fn, "rb");
	if (fp != NULL)
	{
		fclose(fp);
		return 1;
	}
	return GetDownloader(fn);
}

int GetDownloadFileLink(char *content, char *dest)
{
	char *find = strstr(content, "%%%("), htp[] = "http://noxcommunity.com/forum/download/file.php?";

	dest = dest + StrManCopyString(htp, sizeof(htp) - 1, dest);
	if (find != NULL)
	{
		find = find + 4;
		while (*find)
		{
			if (*find == ')')
				break;
			else if (*find == '&')
			{
				*dest++ = *find;
				find = find + 5;
			}
			else
				*dest++ = *find++;
		}
		*dest = 0;
		return 1;
	}
	return 0;
}

int CheckVersionNumber(char *appVers, char *servSrc)
{
	char *servCmp = NULL;

	if (servSrc != NULL)
	{
		servCmp = servSrc + 4;
		return ((appVers[0] == servCmp[0]) && (appVers[1] == servCmp[1]) && (appVers[2] == servCmp[2]) && (appVers[3] == servCmp[3]) && (appVers[4] == servCmp[4]));
	}
	return 0;
}

int CheckAppVersion(HINTERNET hHttpGet, int vers, char *url)
{
	char verStr[40] = { 0, }, dump[8192] = { 0, };
	int getLen = 0;

	sprintf_s(verStr, 40, "@@@@%d<", vers);
	if (InternetReadFile(hHttpGet, dump, 8192, &getLen))
	{
		if (getLen)
		{
			if (!CheckVersionNumber(verStr + 4, strstr(dump, "@@@@")))
			{
				return GetDownloadFileLink(dump, url);
			}
		}
	}
	return 0;
}

int GetUpdateReady(PMASK *pmask)
{
	wchar_t myPage[] = L"forum/viewtopic.php?f=54&t=1432";
	HINTERNET hInternet = InternetOpen(L"MyAgent", PRE_CONFIG_INTERNET_ACCESS, NULL, INTERNET_INVALID_PORT_NUMBER, 0);
	HINTERNET hConnect = InternetConnect(hInternet, L"noxcommunity.com", INTERNET_INVALID_PORT_NUMBER, NULL, NULL, INTERNET_SERVICE_HTTP, 0, 0);
	HINTERNET hHttpGet;
	int upd = 0;

	if (hInternet == NULL) return 0;
	if (hConnect == NULL) return 0;
	hHttpGet = HttpOpenRequest(hConnect, L"GET", myPage, HTTP_VERSION, NULL, 0, INTERNET_FLAG_DONT_CACHE, 0);
	if (hHttpGet != NULL)
	{
		if (HttpSendRequest(hHttpGet, NULL, 0, 0, 0))
		{
			upd = CheckAppVersion(hHttpGet, pmask->lchVer, pmask->patchGet);
		}
	}
	InternetCloseHandle(hInternet);
	InternetCloseHandle(hConnect);
	InternetCloseHandle(hHttpGet);
	return upd;
}

int PatchGetDownload(HINTERNET hHttpGet, HINTERNET hInternet, char *patchExeFn)
{
	char dump[8192] = { 0, }, dest[200] = { 0, };
	int getLen = 0;

	if (InternetReadFile(hHttpGet, dump, 8192, &getLen))
	{
		if (getLen)
		{
			if (GetDownloadFileLink(dump, dest))
				return StartPatchFileDownload(hInternet, dest, patchExeFn);
		}
	}
	return 0;
}

int GetDownloader(char *patchExeFn)
{
	wchar_t myPage[] = L"/forum/viewtopic.php?f=44&t=1412&p=3056#p3056";
	HINTERNET hInternet = InternetOpen(L"MyAgent", PRE_CONFIG_INTERNET_ACCESS, NULL, INTERNET_INVALID_PORT_NUMBER, 0);
	HINTERNET hConnect = InternetConnect(hInternet, L"noxcommunity.com", INTERNET_INVALID_PORT_NUMBER, NULL, NULL, INTERNET_SERVICE_HTTP, 0, 0);
	HINTERNET hHttpGet;
	int res = 0;

	if (hInternet == NULL) return 0;
	if (hConnect == NULL) return 0;
	hHttpGet = HttpOpenRequest(hConnect, L"GET", myPage, HTTP_VERSION, NULL, 0, INTERNET_FLAG_DONT_CACHE, 0);
	if (hHttpGet != NULL)
	{
		if (HttpSendRequest(hHttpGet, NULL, 0, 0, 0))
		{
			res = PatchGetDownload(hHttpGet, hInternet, patchExeFn);
		}
	}
	InternetCloseHandle(hInternet);
	InternetCloseHandle(hConnect);
	InternetCloseHandle(hHttpGet);
	return res;
}

void QuestionDoUpdate(char *updExeName, PMASK *pmask, HWND hWnd)
{
	STARTUPINFO startup_info = { 0, };
	PROCESS_INFORMATION game_process;
	wchar_t fn[200] = { 0, }, url[200] = { 0, }, updFName[40] = { 0, };
	wchar_t execFn[440] = { 0, };
	size_t cn;
	unsigned short key = 23;

	startup_info.cb = sizeof(STARTUPINFO);
	if (CheckUpdateExecFile(updExeName))
	{
		if (GetUpdateReady(pmask))
		{
			if (MessageBox(hWnd, L"지금, 녹스 미니런처의 신규 릴리즈가 있습니다.\n메시지 상자를 닫으시면 프로그램 종료 후 업데이트가 진행됩니다",
				L"미니런처 업데이트", MB_OK | MB_ICONINFORMATION) == IDOK)
			{
				GetModuleFileName(NULL, fn, 200);
				mbstowcs_s(&cn, url, sizeof(url), pmask->patchGet, strlen(pmask->patchGet));
				mbstowcs_s(&cn, updFName, sizeof(updFName), updExeName, strlen(updExeName));
				key += 31;
				wsprintf(execFn, L"%s \"%s\" \"%s\" %c", updFName, fn, url, key);
				if (CreateProcess(NULL, execFn, NULL, NULL, 1, 0, NULL, NULL, &startup_info, &game_process))
				{
					pmask->closePass = 1;
					SendMessage(hWnd, WM_CLOSE, 0, 0);
				}
			}
		}
		else if (pmask->enable_exec)
			ShowNoxUser(pmask, 0);
	}
}

void ShowNoxUser(PMASK *pmask, int clearDraw)
{
	servUser xServ;

	if (clearDraw)
	{
		if (abs(clock() - pmask->sTime) < 10000)
			return;
	}
	xServ.games = 0;
	xServ.users = 0;
	pmask->sTime = clock();
	if (GetNoxServerUsers(&xServ))
	{
		pmask->servUser = xServ.users;
		pmask->servGame = xServ.games;
		DisplayNoxServerUserCount(pmask, clearDraw);
	}
}

DWORD GetServerXOn(HINTERNET hHttpGet, servUser *xServ)
{
	char numberString[20] = { 0, }, *pBuffer = NULL;
	DWORD dwSize = 0, dwRead = 20, dwCor = 0, dwTSize;
	
	if (!HttpQueryInfo(hHttpGet, HTTP_QUERY_CONTENT_LENGTH, numberString, &dwRead, NULL))
		return 0;
	dwTSize = WCharToInt(numberString);
	if (dwTSize && dwTSize < 10000)
	{
		pBuffer = (char *)malloc(sizeof(char) * (dwTSize + 1));
		memset(pBuffer, 0, dwTSize + 1);
		do
		{
			InternetQueryDataAvailable(hHttpGet, &dwSize, 0, 0);
			InternetReadFile(hHttpGet, pBuffer + dwCor, dwSize, &dwRead);
			dwCor += dwRead;
		} while (dwRead);
		xServ->users = CountServerUsers(pBuffer);
		xServ->games = CountServerGames(pBuffer);
		free(pBuffer);
	}
	return dwTSize;
}

int CountServerUsers(char *content)
{
	char *find = strstr(content, "<player rank=");
	if (find != NULL)
	{
		return CountServerUsers(find + 14) + 1;
	}
	else
		return 0;
}

int CountServerGames(char *content)
{
	char *find = strstr(content, "access=\"Open\"");
	if (find != NULL)
	{
		return CountServerGames(find + 5) + 1;
	}
	else
		return 0;
}

int GetNoxServerUsers(servUser *xServ)
{
	wchar_t myPage[] = L"/servers.php ";
	HINTERNET hInternet = InternetOpen(L"HttpRead", PRE_CONFIG_INTERNET_ACCESS, NULL, NULL, 0);
	HINTERNET hConnect = InternetConnect(hInternet, L"www.noxcommunity.com", INTERNET_INVALID_PORT_NUMBER, NULL, NULL, INTERNET_SERVICE_HTTP, 0, 0);
	HINTERNET hHttpGet;
	int res = 0;

	myPage[sizeof(myPage) / 2 - 1] = (short)0;
	if (hInternet == NULL) return 0;
	if (hConnect == NULL) return 0;
	hHttpGet = HttpOpenRequest(hConnect, L"GET", myPage, HTTP_VERSION, NULL, 0, INTERNET_FLAG_DONT_CACHE, 0);
	if (hHttpGet != NULL)
	{
		if (HttpSendRequest(hHttpGet, NULL, 0, 0, 0))
		{
			res = GetServerXOn(hHttpGet, xServ);
		}
	}
	InternetCloseHandle(hHttpGet);
	InternetCloseHandle(hConnect);
	InternetCloseHandle(hInternet);
	return res;
}