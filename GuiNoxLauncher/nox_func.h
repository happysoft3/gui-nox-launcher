#pragma once

#include "head.h"

#define CON_COLOR											0x69fe50
#define CON_STR_ADDR										0x750600
#define CON_SAY_ADDR										0x751000

int InjectNoxMemAlloc(PMASK *pmask, int allocSize);
unsigned WINAPI ThreadNoxCmdLine(void *thread_ptr);
void NoxConsolePrint(PMASK *pmask, char *str, int color, int str_len, char *cmdstr, int cmd_len);
void NoxCallFunction(PMASK *pmask);
void NoxConsolePrintMessage(PMASK *pmask, char txtColor, char* text);
void NoxCmdOutput(PMASK *pmask, char slient, char *content);
void NoxServerKickClient(PMASK *pmask, char pIndex);