#pragma once

#include "head.h"


typedef struct _servXOn
{
	int users;
	int games;
}servUser;

DWORD DownloadFromURL(HINTERNET hInternet, wchar_t *pszURL, char **ptr);
int StartPatchFileDownload(HINTERNET hInternet, char *url, char *fn);
int CheckUpdateExecFile(char *fn);
int GetDownloadFileLink(char *content, char *dest);

int CheckVersionNumber(char *appVers, char *servSrc);
int CheckAppVersion(HINTERNET hHttpGet, int vers, char *url);
int GetUpdateReady(PMASK *pmask);
int PatchGetDownload(HINTERNET hHttpGet, HINTERNET hInternet, char *patchExeFn);
int GetDownloader(char *patchExeFn);
void QuestionDoUpdate(char *updExeName, PMASK *pmask, HWND hWnd);
void ShowNoxUser(PMASK *pmask, int clearDraw);
DWORD GetServerXOn(HINTERNET hHttpGet, servUser *xServ);
int CountServerUsers(char *content);
int CountServerGames(char *content);
int GetNoxServerUsers(servUser *xServ);