#pragma once

#include "head.h"


void CreateMinimizeWindow(PMASK *pmask, HWND hWnd);
void ShowMyMinimizeWindow(PMASK *pmask);
void ShowMainWindow(PMASK *pmask, HWND hDlg);