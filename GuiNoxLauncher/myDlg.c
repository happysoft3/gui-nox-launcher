
#include "head.h"
#include "myDlg.h"
#include "resource.h"
#include "tools.h"


void CopiesNoxExcuteFile(char *selFileName, char *changeFileName, char **dest)
{

}

void SelectNoxPath(HWND hWnd, PMASK *pmask, EDIT_TXT *edit_str)
{
	OPENFILENAME OFN;
	TCHAR lpstrFile[200] = { 0, };
	char all_path[200] = { 0, }, *cptr[3] = { NULL, };

	if (!pmask->nox_exec)
	{
		MessageBox(hWnd, L"NOX 폴더내 녹스 실행파일을 선택해 주세요 (헤쿠바 모양의 아이콘)", L"녹스 경로설정", MB_OK | MB_ICONEXCLAMATION);
		memset(&OFN, 0, sizeof(OPENFILENAME));
		OFN.lStructSize = sizeof(OPENFILENAME);
		OFN.hwndOwner = hWnd;
		OFN.lpstrInitialDir = L".\\";
		OFN.lpstrFilter = L"nox exe file(*.exe*)\0*.exe*\0";
		OFN.lpstrFile = lpstrFile;
		OFN.nMaxFile = 256;
		OFN.Flags = OFN_NOCHANGEDIR;
		if (GetOpenFileName(&OFN) != 0)
		{
			WideCharToMultiByte(CP_ACP, 0, lpstrFile, sizeof(lpstrFile), all_path, sizeof(all_path), NULL, NULL);
			if (all_path[0] && ReadNoxExecFile(all_path))
			{
				GetNoxDirectory(pmask->nox_exe_name, all_path);
				strcpy_s(pmask->nox_path, NOX_PATH_LEN, all_path);
				*cptr = pmask->nox_path;
				*(cptr + 1) = pmask->nox_exe_name;
				*(cptr + 2) = LAUNCHER_FILE_NAME;
				SetTargetPathFile(pmask, cptr);
				pmask->enable_exec = 1;
				SetWindowText(GetDlgItem(hWnd, SETDLG_STATIC1), L"실행파일 지정완료");
			}
			else
			{
				pmask->enable_exec = 0;
				SetWindowText(GetDlgItem(hWnd, SETDLG_STATIC1), L"실행파일 지정에러");
			}
		}
	}
}

BOOL CALLBACK InfoDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
	case WM_INITDIALOG:
		UpdateMyDlg(PROC_MASK[0], hDlg);
		return 1;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED)
		{
			switch (LOWORD(wParam))
			{
			case IDC_BUTTON2:
				SelectNoxPath(hDlg, PROC_MASK[0], PROC_MASK[1]);
				break;
			case IDC_BUTTON1:
				if (!SaveMyDlg(PROC_MASK[0], hDlg))
					break;
			case IDC_BUTTON3:
				AnimateWindow(hDlg, 300, AW_BLEND | AW_HIDE);
				EndDialog(hDlg, 0);
				break;
			case IDC_CHECK2:
				FastModeCheckBox(PROC_MASK[0], hDlg, IDC_CHECK2);
				break;
			case IDC_CHECK5:
				FastModeCheckBox(PROC_MASK[0], hDlg, IDC_CHECK5);
				break;
			case IDC_CHECK6:
				ToggleEditingGreetMessage(PROC_MASK[0], hDlg);
			}
		}
		break;
	}
	return 0;
}

void CreateLauncherOptionDialog(HWND hWnd)
{
	DialogBox(g_hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, InfoDlgProc);
}


void InitDlgList(dList *dLst)
{
	dLst->head = (dNode *)malloc(sizeof(dNode));
	dLst->head->next = NULL;
	dLst->cur = dLst->head;
	dLst->nodeCnt = 0;
}

char AddMyDlgList(dList *dLst, char data)
{
	dNode *latest = (dNode *)malloc(sizeof(dNode));
	latest->stat = data;
	latest->next = NULL;
	dLst->cur->next = latest;
	dLst->cur = latest;
	dLst->nodeCnt++;

	return data;
}

char ReleaseDlgNode(dList *dLst)
{
	dNode *temp = dLst->head->next;
	char res = 0;
	if (temp != NULL)
	{
		res = temp->stat;
		dLst->head->next = temp->next;
		if (temp == dLst->cur)
			dLst->cur = dLst->head;
		free(temp);
		dLst->nodeCnt--;
	}
	return res;
}

void ClearAllDlgList(dNode *pic)
{
	if (pic != NULL)
	{
		ClearAllDlgList(pic->next);
		free(pic);
	}
}

void UpdateGreetMessage(PMASK *pmask, HWND hDlg)
{
	wchar_t wStr[200] = { 0, };

	MultiByteToWideChar(CP_ACP, 0, pmask->flag_ptr->userNotif, (size_t)80, wStr, sizeof(wStr));
	SetDlgItemText(hDlg, IDC_EDIT1, wStr);
}

void ModifGreetMessage(char *greetMsg, int greetLen, HWND hDlg, FILE *output)
{
	wchar_t wStr[200] = { 0, };
	char dest[200] = { 0, };
	int messageLen, len;

	GetDlgItemText(hDlg, IDC_EDIT1, wStr, 200);
	len = StrManGetWordLen(wStr);
	if (len > 10 && len < 100)
	{
		WideCharToMultiByte(CP_ACP, 0, wStr, len + 2, dest, sizeof(dest), NULL, NULL);
		StrManFillByteArray(greetMsg, greetLen, 0);
		messageLen = strlen(dest);
		StrManCopyString(dest, messageLen, greetMsg);
		fwrite(&messageLen, sizeof(int), (size_t)1, output);
		fwrite(dest, sizeof(char), (size_t)messageLen, output);
	}
}

void UpdateMyDlg(PMASK *pmask, HWND hDlg)
{
	if (pmask->flag_ptr->noLimit)
		SendDlgItemMessage(hDlg, IDC_CHECK2, BM_SETCHECK, BST_CHECKED, 0);
	else if (pmask->flag_ptr->noLimitNom)
		SendDlgItemMessage(hDlg, IDC_CHECK5, BM_SETCHECK, BST_CHECKED, 0);
	SendDlgItemMessage(hDlg, IDC_CHECK1, BM_SETCHECK, pmask->flag_ptr->key_share, 0);
	SendDlgItemMessage(hDlg, IDC_CHECK3, BM_SETCHECK, pmask->flag_ptr->warShield, 0);
	SendDlgItemMessage(hDlg, IDC_CHECK4, BM_SETCHECK, pmask->flag_ptr->hide_wnd, 0);
	SendDlgItemMessage(hDlg, IDC_CHECK8, BM_SETCHECK, pmask->flag_ptr->xbowDelay, 0);
	SendDlgItemMessage(hDlg, IDC_CHECK9, BM_SETCHECK, pmask->flag_ptr->hp_bar, 0);
	SendDlgItemMessage(hDlg, IDC_CHECK10, BM_SETCHECK, pmask->flag_ptr->showChatBoard, 0);
	SendDlgItemMessage(hDlg, IDC_CHECK11, BM_SETCHECK, pmask->flag_ptr->hookMapScr, 0);
	if (pmask->flag_ptr->notifUse)
	{
		SendDlgItemMessage(hDlg, IDC_CHECK6, BM_SETCHECK, BST_CHECKED, 0);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT1), 1);
	}
	if (pmask->enable_exec)
		SetWindowText(GetDlgItem(hDlg, SETDLG_STATIC1), L"녹스 실행파일 선택완료");
	else
		SetWindowText(GetDlgItem(hDlg, SETDLG_STATIC1), L"경로설정을 클릭하여 녹스 실행파일을 선택해주세요");
	UpdateGreetMessage(pmask, hDlg);
	AnimateWindow(hDlg, 300, AW_BLEND);
	AnimateWindow(pmask->edit_ptr->main_wnd, 300, AW_BLEND | AW_HIDE);
}

char GetMyCheckBoxStat(dList *dLst, HWND hDlg, int item)
{
	char res = (char)SendDlgItemMessage(hDlg, item, BM_GETCHECK, 0, 0);

	AddMyDlgList(dLst, res);
	return res;
}

int SaveMyDlg(PMASK *pmask, HWND hDlg)
{
	dList dLst;

	dLst.MainPtr = pmask;
	if (pmask->nox_exec)
	{
		MessageBox(hDlg, L"녹스가 실행중인 상태에서 옵션값을 변경할 수 없습니다", L"녹스가 실행중인 상태에서는 허용되지 않음", MB_OK | MB_ICONERROR);
		return 0;
	}
	else
	{
		InitDlgList(&dLst);
		pmask->flag_ptr->key_share = GetMyCheckBoxStat(&dLst, hDlg, IDC_CHECK1);
		pmask->flag_ptr->noLimit = GetMyCheckBoxStat(&dLst, hDlg, IDC_CHECK2);
		pmask->flag_ptr->warShield = GetMyCheckBoxStat(&dLst, hDlg, IDC_CHECK3);
		pmask->flag_ptr->hide_wnd = GetMyCheckBoxStat(&dLst, hDlg, IDC_CHECK4);
		pmask->flag_ptr->noLimitNom = GetMyCheckBoxStat(&dLst, hDlg, IDC_CHECK5);
		pmask->flag_ptr->xbowDelay = GetMyCheckBoxStat(&dLst, hDlg, IDC_CHECK8);
		pmask->flag_ptr->hp_bar = GetMyCheckBoxStat(&dLst, hDlg, IDC_CHECK9);
		pmask->flag_ptr->showChatBoard = GetMyCheckBoxStat(&dLst, hDlg, IDC_CHECK10);
		pmask->flag_ptr->notifUse = GetMyCheckBoxStat(&dLst, hDlg, IDC_CHECK6);
		pmask->flag_ptr->hookMapScr = GetMyCheckBoxStat(&dLst, hDlg, IDC_CHECK11);
		if (pmask->flag_ptr->noLimit)
			sprintf_s(pmask->flag_ptr->cmdLine, (size_t)20, " -nolimit");
		else
			memset(pmask->flag_ptr->cmdLine, 0, 20);
		if ((char)SendDlgItemMessage(hDlg, IDC_CHECK7, BM_GETCHECK, 0, 0))
			ParseUserData(hDlg, &dLst, USER_SETTING_FILE_NAME);
		ClearAllDlgList(dLst.head);
	}
	return 1;
}

void FastModeCheckBox(PMASK *pmask, HWND hDlg, int item)
{
	if ((char)SendDlgItemMessage(hDlg, IDC_CHECK2, BM_GETCHECK, 0, 0) || (char)SendDlgItemMessage(hDlg, IDC_CHECK5, BM_GETCHECK, 0, 0))
	{
		if (item ^ IDC_CHECK2)
		{
			SendDlgItemMessage(hDlg, IDC_CHECK2, BM_SETCHECK, BST_UNCHECKED, 0);
		}
		else
			SendDlgItemMessage(hDlg, IDC_CHECK5, BM_SETCHECK, BST_UNCHECKED, 0);
	}
}

void ToggleEditingGreetMessage(PMASK *pmask, HWND hDlg)
{
	char myIsCheck = (char)SendDlgItemMessage(hDlg, IDC_CHECK6, BM_GETCHECK, 0, 0);

	if (myIsCheck)
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT1), 1);
	else
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT1), 0);
}

void ParseUserData(HWND hDlg, dList *dLst, char *fn)
{
	PMASK *pmask = dLst->MainPtr;
	FILE *fp = NULL;
	char cnt = dLst->nodeCnt;
	int res = 0;

	fopen_s(&fp, fn, "wb");
	if (fp != NULL)
	{
		for (char i = 0; i < cnt; i++)
			res = res | ((ReleaseDlgNode(dLst) & 1) << (int)i);
		fwrite(&res, sizeof(int), (size_t)1, fp);
		if (pmask->flag_ptr->notifUse)
			ModifGreetMessage(pmask->flag_ptr->userNotif, 200, hDlg, fp);
		fclose(fp);
	}
}