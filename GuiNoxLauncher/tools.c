
#include "head.h"
#include "tools.h"
#include "chat_fix.h"


void GetNoxDirectory(char *dst, char *src)
{
	char *find = strstr(src, "\\");

	if (find != NULL)
		GetNoxDirectory(dst, find + 1);
	else
	{
		strcpy_s(dst, (size_t)NOX_EXE_NAME_LEN, src);
		memset(src, 0, strlen(dst));
	}
}

void NoxSysCall(PMASK *pmask)
{
	STARTUPINFOA startup_info = { 0, };
	PROCESS_INFORMATION game_process = { 0, };
	char cmd[200] = { 0, };
	wchar_t exe_local[200] = { 0, };
	
	startup_info.cb = sizeof(STARTUPINFO);
	sprintf_s(cmd, sizeof(cmd), "%s%s", pmask->nox_path, pmask->nox_exe_name);
	if (CreateProcessA(cmd, NULL, NULL, NULL, 0, 0, NULL, pmask->nox_path, &startup_info, &game_process))
	{
		pmask->pground = game_process.hProcess;
		WaitForSingleObject(game_process.hProcess, INFINITE);
		CloseHandle(game_process.hProcess);
		CloseHandle(game_process.hThread);
	}
}

void AppendText(EDIT_TXT *edit_str, LPCWSTR text)
{
	int idx = GetWindowTextLength(edit_str->hEdit);

	SendMessage(edit_str->hEdit, EM_SETSEL, (WPARAM)idx, (LPARAM)idx);
	SendMessage(edit_str->hEdit, EM_REPLACESEL, 0, (LPARAM)text);
}

void MyStringCopy(char *dst, char *src)
{
	int len;

	if (src != NULL)
	{
		len = strlen(src);
		for (int i = 0; i < len; i++)
		{
			if (src[i] == '\n') break;
			dst[i] = src[i];
		}
	}
}

int SetTargetPathFile(PMASK *pmask, char **cptr)
{
	FILE *fp = NULL;

	fopen_s(&fp, cptr[2], "wt");
	if (fp != NULL)
	{
		fwrite(cptr[0], sizeof(char), strlen(cptr[0]), fp);
		fprintf(fp, "\n");
		fwrite(cptr[1], sizeof(char), strlen(cptr[1]), fp);
		fclose(fp);
		return 1;
	}
	return 0;
}

int GetTargetPathFile(PMASK *pmask, char *filename)
{
	FILE *fp = NULL;
	char read[200] = { 0, };

	fopen_s(&fp, filename, "r");
	if (fp != NULL)
	{
		MyStringCopy(pmask->nox_path, fgets(read, sizeof(read), fp));
		MyStringCopy(pmask->nox_exe_name, fgets(read, sizeof(read), fp));
		fclose(fp);
		return 1;
	}
	return 0;
}

int SearchProcessID(PMASK *pmask)
{
	pmask->hwnd = FindWindow(NULL, TEXT("Nox GUI"));
	if (pmask->hwnd)
	{
		GetWindowThreadProcessId(pmask->hwnd, &pmask->process_id);
		pmask->pground = OpenProcess(MAXIMUM_ALLOWED, 0, pmask->process_id);
		if (pmask->pground)
		{
			CloseHandle(pmask->pground);
			return 1;
		}
	}
	return 0;
}

void SaveApplicationDirectory(PMASK *pmask)
{
	GetCurrentDirectory(NOX_PATH_LEN, pmask->cur_path);
}

int ReadNoxExecFile(char *fn)
{
	FILE *fp = NULL;
	int read, res = 0;

	fopen_s(&fp, fn, "rb");
	if (fp != NULL)
	{
		fseek(fp, 0x187068, 0);
		fread_s(&read, sizeof(int), sizeof(char), sizeof(int), fp);
		if (read == 0x2e786f6e)
			res = 1;
		fclose(fp);
	}
	return res;
}

int CheckNoxExecFile(PMASK *pmask)
{
	char all_path[200] = { 0, };

	sprintf_s(all_path,sizeof(all_path),  "%s%s", pmask->nox_path, pmask->nox_exe_name);
	if (ReadNoxExecFile(all_path))
		return 1;
	return 0;
}

int GetMemory(PMASK *pmask, int target)
{
	int res = 0;

	if (!ReadProcessMemory(pmask->pground, (LPCVOID)target, (LPVOID)&res, sizeof(DWORD), NULL))
		return 0;
	return res;
}

short GetMemoryWord(PMASK *pmask, int target)
{
	short res = 0;

	if (!ReadProcessMemory(pmask->pground, (LPCVOID)target, (LPVOID)&res, sizeof(short), NULL))
		return 0;
	return res;
}

int ReadMemString(PMASK *pmask, int ptr, char *buf, int len)
{
	return ReadProcessMemory(pmask->pground, (LPCVOID)ptr, (LPVOID)buf, (size_t)len, NULL);
}

void SetMemory(PMASK *pmask, int target, int value)
{
	WriteProcessMemory(pmask->pground, (LPVOID)target, (LPCVOID)&value, sizeof(DWORD), NULL);
}

void SetMemoryWord(PMASK *pmask, int target, short value)
{
	WriteProcessMemory(pmask->pground, (LPVOID)target, (LPCVOID)&value, sizeof(short), NULL);
}

void WriteMemString(PMASK *pmask, int target, char *buf, int len)
{
	WriteProcessMemory(pmask->pground, (LPVOID)target, (LPCVOID)buf, (size_t)len, NULL);
}

int IsHostPlayer(PMASK *pmask)
{
	return GetMemory(pmask, HOST_PLAYER_ADDR);
}

int CheckQuestModeFlag(PMASK *pmask)
{
	return !(GetMemory(pmask, 0x85b7a0) & 0xff);
}

int GetUserDataFileSize(FILE *fp)
{
	int size;

	fseek(fp, 0, 2);
	size = ftell(fp);
	fseek(fp, 0, 0);
	return size;
}

void GetUserNotifyMessage(PMASK *pmask, FILE *setFile)
{
	int messageLen = 0;

	memset(pmask->flag_ptr->userNotif, 0, (size_t)80);
	fread_s(&messageLen, sizeof(int), sizeof(int), (size_t)1, setFile);
	if (messageLen >= GREET_MSG_FILE_MIN_SIZE && messageLen < 80)
		fread_s(pmask->flag_ptr->userNotif, (size_t)80, sizeof(char), (size_t)messageLen, setFile);
}


void TestPrintMessage(HWND hWnd, int val, char *str)
{
	/*wchar_t wstr[400] = { 0, };
	size_t cn;

	if (str == NULL)
	{
		wsprintf(wstr, L"test value: %d", val);
	}
	else
		mbstowcs_s(&cn, wstr, 400, str, strlen(str));
	MessageBox(hWnd, wstr, MESSAGE_BOX_NAME, MB_OK | MB_ICONINFORMATION);*/
}

void TestOutputFile(char *fn, char *content, int size)
{
	FILE *fp = NULL;

	fopen_s(&fp, fn, "wb");
	if (fp != NULL)
	{
		fwrite(content, sizeof(char), size, fp);
		fclose(fp);
	}
}

void DisplayNoxServerUserCount(PMASK *pmask, int clearDraw)
{
	SYSTEMTIME systime;
	wchar_t wstr[100] = { 0, };

	if (pmask->servUser)
	{
		GetLocalTime(&systime);
		wsprintf(wstr, 
			L"updated time: %d.%02d.%02d. %02d:%02d:%02d -- %d Users, %d Games", 
			systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond, pmask->servUser, pmask->servGame);
		SetWindowText(pmask->edit_ptr->hMainStatic, wstr);
	}
}

int IsNumberC(char co)
{
	return ((co >= '0') && (co <= '9'));
}

int WCharToInt(char *ptr)
{
	char num[10] = { 0, };

	for (int i = 0; i < 10; i++)
	{
		if (IsNumberC(ptr[i * 2]))
			num[i] = ptr[i * 2];
	}
	return atoi(num);
}

wchar_t *CharToWChar(char *src, int len)
{
	wchar_t *dest = NULL;
	int nLen;

	if (src != NULL && len)
	{
		dest = (short *)malloc(sizeof(short) * (len + 1));
		memset(dest, 0, sizeof(short) * (len + 1));
		nLen = MultiByteToWideChar(CP_ACP, 0, src, strlen(src), NULL, 0);
		MultiByteToWideChar(CP_ACP, 0, src, strlen(src), dest, nLen);
	}
	return dest;
}

int StoreDownloadedFile(char *fn, char *pBuffer, int size)
{
	FILE *fp = NULL;

	fopen_s(&fp, fn, "wb");
	if (fp != NULL)
	{
		fwrite(pBuffer, sizeof(char), size, fp);
		fclose(fp);
		return 1;
	}
	return 0;
}

int StrManGetLen(char *str)
{
	for (int i = 0; i < 1024; i++)
	{
		if (str[i])
			continue;
		else
			return i;
	}
	return 0;
}

int StrManCopyString(char *src, int srcLen, char *dest)
{
	int i;

	for (i = 0; i < srcLen; i++)
		dest[i] = src[i];
	return i;
}

int StrManAllMatching(char *src, int srcLen, char *comp)
{
	for (int i = 0; i < srcLen; i++)
	{
		if (comp[i] ^ src[i])
			return 0;
	}
	return 1;
}

void StrManWriteNumber(char *dest, int num)
{
	int *ptr = (int *)dest;

	*ptr = num;
}

void StrManWriteTime(char *dest)
{
	SYSTEMTIME sysTime;

	GetLocalTime(&sysTime);
	StrManWriteNumber(dest, sysTime.wYear | (sysTime.wMonth << 0x10));
	StrManWriteNumber(dest + 4, sysTime.wDay | (sysTime.wHour << 0x10));
	StrManWriteNumber(dest + 8, sysTime.wMinute | (sysTime.wSecond << 0x10));
}

void WriteStreamOnFile(char *fName, char *stream, int streamLen)
{
	StoreDownloadedFile(fName, stream, streamLen);
}

char ExtractRandomByte(int min, int max)
{
	return (rand() % (max - min + 1)) + min;
}

void MarbleFillStream(char *stream, int streamLen, int arg)
{
	int min = arg & 0xffff, max = arg >> 0x10;
	for (int i = 0; i < streamLen; i++)
		stream[i] = ExtractRandomByte(min, max);
}

void StrManFillByteArray(char *stream, int streamLen, int value)
{
	for (int i = 0; i < streamLen; i++)
		stream[i] = value;
}

void *MyMemAlloc(int size)
{
	void *alloc = malloc((size_t)size);
	
	MarbleFillStream(alloc, size, 0xff << 0x10);
	return alloc;
}

int StrManGetWordLen(wchar_t *str)
{
	for (int i = 0; ; i++)
	{
		if (!str[i]) return i;
	}
	return 0;
}

