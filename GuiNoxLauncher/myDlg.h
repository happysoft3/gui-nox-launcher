#pragma once

#include "head.h"

typedef struct _dlgNode
{
	char stat;
	struct _dlgNode *next;
}dNode;

typedef struct _dlgList
{
	void *MainPtr;
	char nodeCnt;
	struct _dlgNode *head;
	struct _dlgNode *cur;
}dList;


void CreateLauncherOptionDialog(HWND hWnd);
void InitDlgList(dList *dLst);
char AddMyDlgList(dList *dLst, char data);
char ReleaseDlgNode(dList *dLst);
void ClearAllDlgList(dNode *pic);

void UpdateGreetMessage(PMASK *pmask, HWND hDlg);
void ModifGreetMessage(char *greetMsg, int greetLen, HWND hDlg, FILE *output);
void UpdateMyDlg(PMASK *pmask, HWND hDlg);
char GetMyCheckBoxStat(dList *dLst, HWND hDlg, int item);
int SaveMyDlg(PMASK *pmask, HWND hDlg);

void FastModeCheckBox(PMASK *pmask, HWND hDlg, int item);
void ToggleEditingGreetMessage(PMASK *pmask, HWND hDlg);
void ParseUserData(HWND hDlg, dList *dLst, char *fn);