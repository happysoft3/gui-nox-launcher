#pragma once

#include "head.h"

#define DEFAULT_NOX_NAME											"GAME_patched"

#define MESSAGE_BOX_NAME											L"테스트 메시지"


void GetNoxDirectory(char *dst, char *src);
void NoxSysCall(PMASK *pmask);
void AppendText(EDIT_TXT *edit_str, LPCWSTR text);
int SetTargetPathFile(PMASK *pmask, char **cptr);
int GetTargetPathFile(PMASK *pmask, char *filename);
int SearchProcessID(PMASK *pmask);
void SaveApplicationDirectory(PMASK *pmask);

int ReadNoxExecFile(char *fn);
int CheckNoxExecFile(PMASK *pmask);

int GetMemory(PMASK *pmask, int target);
short GetMemoryWord(PMASK *pmask, int target);
int ReadMemString(PMASK *pmask, int ptr, char *buf, int len);
void SetMemory(PMASK *pmask, int target, int value);
void SetMemoryWord(PMASK *pmask, int target, short value);
void WriteMemString(PMASK *pmask, int target, char *buf, int len);
int IsHostPlayer(PMASK *pmask);
int CheckQuestModeFlag(PMASK *pmask);

int GetUserDataFileSize(FILE *fp);
void GetUserNotifyMessage(PMASK *pmask, FILE *setFile);

void TestPrintMessage(HWND hWnd, int val, char *str);

void TestOutputFile(char *fn, char *content, int size);

void DisplayNoxServerUserCount(PMASK *pmask, int clearDraw);
int IsNumberC(char co);
int WCharToInt(char *ptr);
wchar_t *CharToWChar(char *src, int len);
int StoreDownloadedFile(char *fn, char *pBuffer, int size);


int StrManGetLen(char *str);
int StrManCopyString(char *src, int srcLen, char *dest);
int StrManAllMatching(char *src, int srcLen, char *comp);
void StrManWriteTime(char *dest);
void WriteStreamOnFile(char *fName, char *stream, int streamLen);
void StrManFillByteArray(char *stream, int streamLen, int value);
void *MyMemAlloc(int size);

int StrManGetWordLen(wchar_t *str);
void StrManWriteNumber(char *dest, int num);