

#include "ladderFile.h"
#include "tools.h"
#include "NoxCryptApi.h"

void LadderFileName(char *userId, char **dest)
{
	char *fileName = NULL, exp[] = ".usr", path[] = "users\\";
	int len = StrManGetLen(userId), temp;

	if (len)
	{
		fileName = (char *)malloc((size_t)(len) + sizeof(exp) + sizeof(path));
		temp = StrManCopyString(path, sizeof(path) - 1, fileName);
		temp += StrManCopyString(userId, len, fileName + temp);
		StrManCopyString(exp, sizeof(exp), fileName + temp);
		*dest = fileName;
	}
}

void RemoveTestCode(char *fName, int fSize)
{
	char buff[255] = { 0, };
	FILE *fp = NULL;

	fopen_s(&fp, fName, "rb");
	fread_s(buff, fSize, fSize, sizeof(char), fp);
	WriteStreamOnFile("dedfjhc.txt", buff, fSize);
	fclose(fp);
}

int LadderFileLoad(FILE **fp, char *fName)
{
	int fSize = 0;

	fopen_s(fp, fName, "rb");
	if (*fp != NULL)
	{
		fSize = GetUserDataFileSize(*fp);
		if ((!(fSize % 16)) && fSize)
		{
			return fSize;
		}
		fclose(*fp);
	}
	return 0;
}

int LadderFileCheckUserId(char *stream, char *userId)
{
	char userIdLen = (char)stream[0];

	return StrManAllMatching(stream + 1, (int)userIdLen, userId) * (int)(userIdLen);
}

int LadderDataAnalysis(char *stream, char *userId)
{
	int *header = (int *)stream;

	if (*header == 0xd2adface)
		return LadderFileCheckUserId(stream + 4, userId);
	return 0;
}

int LadderDataPick(char *stream, int offset, LadderClass *ladder)
{
	int *readValue = NULL;

	if (!offset) return 0;
	offset += 5;
	readValue = (int *)(stream + offset);
	ladder->allKills = readValue[0];
	ladder->allDeath = readValue[1];
	ladder->allGens = readValue[2];
	ladder->allSecret = readValue[3];
	ladder->allMons = readValue[4];
	ladder->allQDeath = readValue[5];
	ladder->allGameCount = readValue[6];
	ladder->allVictory = readValue[7];
	ladder->allDefeat = readValue[8];
	if (ladder->allDefeat > ladder->allGameCount)
		ladder->allDefeat = 0;
	return 1;
}

int ReadLadderFile(FILE *fp, LadderClass *ladder, int fSize)
{
	char *stream = NULL, *destream = NULL;

	if ((fSize >> 0x10) || (fSize < 200))
		return 0;
	stream = (char *)malloc((size_t)(fSize + 1));
	fread_s(stream, (size_t)fSize, (size_t)fSize, sizeof(char), fp);
	destream = NoxAPICrypt(stream, NOX_GAMEDATA, fSize, 0);
	free(stream);
	fclose(fp);
	return LadderDataPick(destream, LadderDataAnalysis(destream, ladder->userId), ladder);
}

int LoadLadderData(LadderClass *ladder)
{
	FILE *fp = NULL;
	char *fName = NULL;
	int fSize;

	LadderFileName(ladder->userId, &fName);
	if (fName != NULL)
	{
		fSize = LadderFileLoad(&fp, fName);
		if (fSize && fp != NULL)
		{
			if (ReadLadderFile(fp, ladder, fSize))
				return 1;
			fclose(fp);
		}
		free(fName);
	}
	return 0;
}

int WriteLadderData(LadderClass *ladder, char **dest)
{
	char userIdLen = StrManGetLen(ladder->userId);
	int streamLen = (192 + (userIdLen * 16));
	char *stream = (char *)MyMemAlloc((size_t)streamLen), *output = NULL;
	int *header = (int *)stream;
	int *assign = (int *)(stream + (userIdLen + 5));

	*header = 0xd2adface;
	stream[4] = userIdLen;
	StrManCopyString(ladder->userId, (int)userIdLen, stream + 5);
	assign[0] = ladder->allKills;
	assign[1] = ladder->allDeath;
	assign[2] = ladder->allGens;
	assign[3] = ladder->allSecret;
	assign[4] = ladder->allMons;
	assign[5] = ladder->allQDeath;
	assign[6] = ladder->allGameCount;
	assign[7] = ladder->allVictory;
	assign[8] = ladder->allDefeat;
	StrManWriteTime((char *)&assign[9]);
	output = NoxAPICrypt(stream, NOX_GAMEDATA, streamLen, 1);

	free(stream);
	*dest = output;

	return streamLen;
}

void SaveLadderData(LadderClass *ladder)
{
	char *fName = NULL, *data = NULL;
	int streamLen;

	LadderFileName(ladder->userId, &fName);
	if (fName != NULL)
	{
		streamLen = WriteLadderData(ladder, &data);
		WriteStreamOnFile(fName, data, streamLen);
		if (data != NULL)
			free(data);
		free(fName);
	}
}