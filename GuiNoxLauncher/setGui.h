#pragma once

#include "head.h"


void SetMyButtonPaint(EDIT_TXT *mWndPtr);

void StartupMessage(HWND hWnd, PMASK *pmask, EDIT_TXT *edit_str);
void DrawNoxStatus(PMASK *pmask, HWND hWnd);
void UpdateNoxStatTab(PMASK *pmask);

int CheckMainWnd(PMASK *pmask, HWND hWnd);
LRESULT StaticCustomBrush(EDIT_TXT *mWnd, HDC hdc);
LRESULT SetMainEditColor(PMASK *pmask, WPARAM wParam);
LRESULT SetLauncherButtonColor(PMASK *pmask, WPARAM wParam);

//void InitMiniDlg(PMASK *pmask, HWND hDlg);
//BOOL CALLBACK MiniDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
//void CreateMinimizeWindow(PMASK *pmask, HWND hWnd);
//void ShowMyMinimizeWindow(PMASK *pmask);
//void ShowMainWindow(PMASK *pmask, HWND hDlg);

void CreateMyTimer(PMASK *pmask, HWND hWnd);
void MyWriteCurrentTime(PMASK *pmask, int target);
void CALLBACK MyTimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);
void RemoveMyTimer(PMASK *pmask, HWND hWnd);

//void *ToggleMyDlgColor(PMASK *pmask, HWND hDlg);
HBRUSH CreateGradientBrush(COLORREF top, COLORREF bottom, LPNMCUSTOMDRAW item);
int OnNotifyHandler(HWND hwnd, PMASK *mProc, LPNMHDR some_item);