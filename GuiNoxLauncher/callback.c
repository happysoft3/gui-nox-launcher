
#include "head.h"
#include "nox_proc.h"
#include "myDlg.h"
#include "setGui.h"
#include "autoUpdate.h"
#include "miniWnd.h"
#include "module\ctrlDraw.h"



void LauncherOnClose(HWND hWnd, PMASK *pmask)
{
	SetWindowLong(pmask->edit_ptr->hEdit, GWLP_WNDPROC, (LONG)oldEditProc);

	DeleteObject(pmask->edit_ptr->mStaticBrush);
	DeleteObject(pmask->edit_ptr->hFont);
	DeleteObject(pmask->edit_ptr->bFont);
	DeleteObject(pmask->edit_ptr->hFont2);
	DeleteObject(pmask->edit_ptr->hBrush1);
	DeleteObject(pmask->edit_ptr->bigFont1);
	RemoveMyTimer(pmask, hWnd);
}

void TeleportWindowAtCenter(HWND hWnd)
{
	RECT rc;

	GetWindowRect(hWnd, &rc);

	int xPos = (GetSystemMetrics(SM_CXSCREEN) - (rc.right - rc.left)) / 2;
	int yPos = (GetSystemMetrics(SM_CYSCREEN) - (rc.bottom - rc.top)) / 2;

	SetWindowPos(hWnd, HWND_BOTTOM, xPos, yPos, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	EDIT_TXT *mWnd = PROC_MASK[1];
	PMASK *mProc = PROC_MASK[0];

	switch (iMessage)
	{
	case WM_CREATE:
		TeleportWindowAtCenter(hWnd);
		StartupMessage(hWnd, mProc, mWnd);
		CreateMinimizeWindow(mProc, hWnd);
		QuestionDoUpdate("GetPatchClass3.exe", mProc, hWnd);
		break;
	case WM_DESTROY:
		ThisShutdown(mProc);
		PostQuitMessage(0);
		return 0;
	case WM_SIZE:
		if (wParam == SIZE_MINIMIZED)
			ShowMyMinimizeWindow(mProc);
		break;
	case WM_PAINT:
		DrawNoxStatus(mProc, hWnd);
		break;
	case WM_CLOSE:
		switch (EndProgram(mProc, hWnd))
		{
		case 0:
			return 0;
		case 1:
			LauncherOnClose(hWnd, mProc);
			break;
		}
		break;
	case WM_DRAWITEM:
		OnDrawItem(hWnd, (DRAWITEMSTRUCT *)lParam);
		break;
	case WM_NOTIFY:
		return OnNotifyHandler(hWnd, mProc, (LPNMHDR)lParam);
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case 0:
			if (CheckNoxPathFile(hWnd))
				RunNox(hWnd, mProc, mWnd);
			break;
		case 1:
			CreateLauncherOptionDialog(hWnd);
			ShowMainWindow(mProc, 0);
			break;
		case 2:
			OpenNoxDirectory(mProc);
			break;
		case 3:
			OpenXwisUsers(mProc);
			break;
		case 4:
			ShowNoxUser(mProc, 1);
			break;
		case 6:
			OpenNaverCafe(mProc);
		}
		break;
	case WM_CTLCOLORBTN: //In order to make those edges invisble when we use RoundRect(),
	{                //we make the color of our button's background match window's background
		return (LRESULT)GetSysColorBrush(COLOR_WINDOW + 1);
	}
	case WM_CTLCOLORSTATIC:
		if (CheckMainWnd(mProc, (HWND)lParam))
			return SetMainEditColor(mProc, wParam);
		else
			return StaticCustomBrush(mWnd, (HDC)wParam);
	}
	return (DefWindowProc(hWnd, iMessage, wParam, lParam));
}