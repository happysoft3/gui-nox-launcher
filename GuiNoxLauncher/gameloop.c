
#include "head.h"
#include "gameloop.h"
#include "chat_fix.h"
#include "user_code.h"
#include "tools.h"
#include "nox_proc.h"
#include "ladderSys.h"


int CheckNoxProcess(PMASK *pmask)
{
	DWORD trash;
	return ReadProcessMemory(pmask->pground, (LPCVOID)CHECK_IN_GAME, &trash, sizeof(DWORD), NULL);
}

void GameOnJoin(PMASK *pmask)
{
	GameLoopOnJoinToServer(pmask);
	NotifyLauncherVer(pmask);
	DrawChatSlot(pmask);
}

void GameOnExit(PMASK *pmask)
{
	int reset = 0;
	GameLoopOnExitToServer(pmask);
	SetMemory(pmask, 0x69c9c8, 0);
	SetMemory(pmask, 0x69f490, 0);
	WriteMemString(pmask, 0x58d875, (char *)&reset, 2);
}

void GameOnLoop(PMASK *pmask)
{
	AlwaysKeySharing(pmask);
	DisplayHealthBar(pmask);
	GameLoopOnGame((LadderClass *)pmask->userLadder);
}

void LoopReadProcess(PMASK *pmask)
{
	ARG_LIST arglist;
	int ptr = 0,  prevStat = 0;

	pmask->arglist_ptr = &arglist;

	if (ReadProcessMemory(pmask->pground, (LPCVOID)RUL_FORMAT_ADDR, NULL, 0, NULL))
	{
		ExecUserCodeFile(pmask, pmask->edit_ptr);
		InitNoxChat(pmask);
		SetGameFrameLimit(pmask, 0);
		while (ReadProcessMemory(pmask->pground, (LPCVOID)CUR_PLAYER_PTR, &ptr, sizeof(DWORD), NULL))
		{
			//FixChatBug(pmask); //TODO: 채팅창 입력 버그 개선
			if (ptr)
			{
				if (GetMemory(pmask, 0x69c9c8))
				{
					if (prevStat ^ ptr)
					{
						prevStat = ptr;
						GameOnJoin(pmask);
					}
					else
						GameOnLoop(pmask);
				}
			}
			else if (prevStat)
			{
				prevStat = ptr;
				GameOnExit(pmask);
			}
			if (pmask->threadExit)
				break;
			Sleep(2);
		}
	}
}