
#include "head.h"
#include "nox_func.h"
#include "tools.h"

int InjectNoxMemAlloc(PMASK *pmask, int allocSize)
{
	BYTE codes[] = {
		0x55, 0x8B, 0x2D, 0x00, 0x06, 0x75, 0x00, 0x68, 0x60, 0x35, 0x40, 0x00, 0x55, 0xFF, 
		0x54, 0x24, 0x04, 0x83, 0xC4, 0x08, 0xA3, 0x00, 0x06, 0x75, 0x00, 0x5D, 0xC3, 0x90
	};
	int ptr = 0x69C9C8 + 12;
	HANDLE hThread;

	SetMemory(pmask, 0x750600, allocSize);
	WriteMemString(pmask, ptr, codes, sizeof(codes));
	hThread = CreateRemoteThread(pmask->pground, NULL, 0, (LPTHREAD_START_ROUTINE)((void *)ptr), NULL, CREATE_SUSPENDED, NULL);
	ResumeThread(hThread);
	WaitForSingleObject(hThread, 2000);

	return GetMemory(pmask, 0x750600);
}

unsigned WINAPI ThreadNoxCmdLine(void *thread_ptr)
{
	__asm
	{
		push 0x450b90
		mov eax, CON_STR_ADDR
			mov eax, [eax]
			push eax
			mov eax, CON_COLOR
			mov eax, [eax]
			push eax
			call[esp + 8]
			add esp, 0xc
			push 0x443c80
			push 1
			mov eax,CON_SAY_ADDR
			mov eax,[eax]
			push eax
			call [esp+8]
			add esp,0xc
			xor eax,eax
	};
	return 0;
}

void NoxConsolePrint(PMASK *pmask, char *str, int color, int str_len, char *cmdstr, int cmd_len)
{
	pmask->arglist_ptr->func_len = (DWORD)NoxConsolePrint - (DWORD)ThreadNoxCmdLine;
	pmask->arglist_ptr->lpstr_ptr = str;
	pmask->arglist_ptr->lpstr_size = str_len;
	pmask->arglist_ptr->lpstr_cmd_ptr = cmdstr;
	pmask->arglist_ptr->lpstr_cmd_size = cmd_len;
	pmask->arglist_ptr->color = color;
	pmask->arglist_ptr->nox_func_addr = ThreadNoxCmdLine;
	NoxCallFunction(pmask);
}

void NoxCallFunction(PMASK *pmask)
{
	wchar_t w_str[400] = { 0, };
	void *str_arg = VirtualAllocEx(pmask->pground, NULL, sizeof(w_str), MEM_COMMIT, PAGE_READWRITE),
		*func_addr = VirtualAllocEx(pmask->pground, NULL, (SIZE_T)pmask->arglist_ptr->func_len, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	char *cmd_arg = (char *)str_arg + 400;
	HANDLE hThread;

	MultiByteToWideChar(CP_ACP, 0, pmask->arglist_ptr->lpstr_ptr, pmask->arglist_ptr->lpstr_size, w_str, sizeof(short) * 200);
		//front index 400: consoleprint
	MultiByteToWideChar(CP_ACP, 0, pmask->arglist_ptr->lpstr_cmd_ptr, pmask->arglist_ptr->lpstr_cmd_size, w_str + 200, sizeof(short) * 200);
		//back index 400: create_chat_bubble_message_from_console_command

	WriteProcessMemory(pmask->pground, (LPVOID)str_arg, (LPCVOID)w_str, sizeof(w_str), NULL);			//write_all_unicode_strings
	WriteProcessMemory(pmask->pground, (LPVOID)CON_STR_ADDR, (LPCVOID)&str_arg, sizeof(int), NULL);		//link consoleprint
	WriteProcessMemory(pmask->pground, (LPVOID)CON_SAY_ADDR, (LPCVOID)&cmd_arg, sizeof(int), NULL);		//link say

	WriteProcessMemory(pmask->pground, (LPVOID)CON_COLOR, (LPCVOID)&pmask->arglist_ptr->color, sizeof(int), NULL);	//font_color
	WriteProcessMemory(pmask->pground, (LPVOID)func_addr, (LPCVOID)pmask->arglist_ptr->nox_func_addr, (SIZE_T)pmask->arglist_ptr->func_len, NULL);
	hThread = CreateRemoteThread(pmask->pground, NULL, 0, (LPTHREAD_START_ROUTINE)func_addr, NULL, CREATE_SUSPENDED, NULL);
	ResumeThread(hThread);
	WaitForSingleObject(hThread, 2000);
	VirtualFreeEx(pmask->pground, str_arg, 0, MEM_RELEASE);
	VirtualFreeEx(pmask->pground, func_addr, 0, MEM_RELEASE);
}

void StrManMultiByteToWChar(char *src, char **dest)
{
	int srcLen = StrManGetLen(src) + 1;
	int nLen = MultiByteToWideChar(CP_ACP, 0, src, srcLen, NULL, 0) * 2 + 1;
	char *res = (char *)malloc((size_t)nLen);

	StrManFillByteArray(res, nLen, 0);
	MultiByteToWideChar(CP_ACP, 0, src, srcLen, (wchar_t *)res, nLen);
	*dest = res;
}

void FixCallOpcode(char *codePos, char *codeStream, int targetAddr)
{
	char **p = (char **)(codePos + 1);
	//return targetAddr - curAddr - 5;

	*p = (char *)(targetAddr - 5 - (int)codeStream);
}

void FixCodeOffset(char *codeStream, char offset, int value)
{
	char **p = (char **)(codeStream + offset);

	*p = (char *)value;
}

void NoxConsolePrintMessage(PMASK *pmask, char txtColor, char* text)
{
	int txtLen = (StrManGetLen(text) + 1) * 2;
	char codes[] = {
		0x50, 0x51, 0xB8, 0x80, 0xAC, 0x80, 0xAC, 0x83, 0xEC, 0x08, 0x31, 0xC9, 0x8A, 
		0x08, 0x89, 0x0C, 0x24, 0x8D, 0x40, 0x01, 0x89, 0x44, 0x24, 0x04, 0xE8, 0x73, 
		0xFB, 0xCF, 0xFF, 0x83, 0xC4, 0x08, 0x59, 0x58, 0xC3
	};
	int codeStart = sizeof(int) + 1 + txtLen;
	char *vStream = VirtualAllocEx(pmask->pground, NULL, sizeof(codes) + codeStart, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	char *codeStream = vStream + codeStart;
	char *uniMsg = NULL;
	HANDLE pExec;

	if (vStream != NULL)
	{
		StrManMultiByteToWChar(text, &uniMsg);
		WriteMemString(pmask, (int)(vStream + 1), uniMsg, txtLen);
		free(uniMsg);
		FixCallOpcode(codes + 0x18, codeStream + 0x18, 0x450b90);
		FixCodeOffset(codes, 3, (int)vStream);
		WriteMemString(pmask, (int)vStream, &txtColor, 1);
		WriteMemString(pmask, (int)codeStream, codes, sizeof(codes));
		pExec = CreateRemoteThread(pmask->pground, NULL, 0, (LPTHREAD_START_ROUTINE)codeStream, NULL, 0, NULL);

		WaitForSingleObject(pExec, 2000);

		VirtualFreeEx(pmask->pground, vStream, 0, MEM_RELEASE);
	}
}

void NoxCmdOutput(PMASK *pmask, char slient, char *content)
{
	int txtLen = (StrManGetLen(content) + 1) * 2;
	char codes[] = {
		0x50, 0x51, 0xB8, 0x80, 0xAC, 0x80, 0xAC, 0x83, 0xEC, 0x08, 0x31, 0xC9, 0x8A, 0x08, 0x89, 0x4C, 0x24, 0x04, 0x8D, 
		0x40, 0x01, 0x89, 0x04, 0x24, 0xE8, 0x63, 0x2C, 0xCF, 0xFF, 0x83, 0xC4, 0x08, 0x59, 0x58, 0xC3
	};
	int codeStart = sizeof(int) + 1 + txtLen;
	char *vStream = VirtualAllocEx(pmask->pground, NULL, sizeof(codes) + codeStart, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	char *codeStream = vStream + codeStart;
	char *uniMsg = NULL;
	HANDLE pExec;

	if (vStream != NULL)
	{
		StrManMultiByteToWChar(content, &uniMsg);
		WriteMemString(pmask, (int)(vStream + 1), uniMsg, txtLen);
		free(uniMsg);
		FixCallOpcode(codes + 0x18, codeStream + 0x18, 0x443c80);
		FixCodeOffset(codes, 3, (int)vStream);
		WriteMemString(pmask, (int)vStream, &slient, 1);
		WriteMemString(pmask, (int)codeStream, codes, sizeof(codes));
		pExec = CreateRemoteThread(pmask->pground, NULL, 0, (LPTHREAD_START_ROUTINE)codeStream, NULL, 0, NULL);

		WaitForSingleObject(pExec, 2000);

		VirtualFreeEx(pmask->pground, vStream, 0, MEM_RELEASE);
	}
}

void NoxNetClientSend(PMASK *pmask, char pIndex, char *packet, char packetSize)
{
	char codes[] = {
		0x50, 0x57, 0x56, 0xB8, 0xAC, 0x99, 0xAC, 0x99, 0x51, 0x31, 0xC9, 0x83, 0xEC, 0x10, 0x8A, 0x08, 0x89, 0x0C, 0x24, 0x8A, 
		0x48, 0x01, 0x89, 0x4C, 0x24, 0x04, 0x8A, 0x48, 0x02, 0x89, 0x4C, 0x24, 0x0C, 0x8D, 0x40, 0x03, 0x89, 0x44, 0x24, 0x08,
		0xE8, 0x93, 0xDB, 0xCB, 0xFF, 0x83, 0xC4, 0x10, 0x59, 0x5E, 0x5F, 0x58, 0xC3
	};
	int codeStart = 3 + packetSize;
	char *vStream = VirtualAllocEx(pmask->pground, NULL, sizeof(codes) + codeStart, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	char *codeStream = vStream + codeStart;
	HANDLE pExec;

	FixCallOpcode(codes + 0x28, codeStream + 0x28, 0x40ebc0);
	FixCodeOffset(codeStream, 4, (int)vStream);
	vStream[0] = pIndex;
	vStream[1] = 1;
	vStream[2] = packetSize;
	WriteMemString(pmask, (int)(vStream + 3), packet, packetSize);
	//pIndex(byte), 0, buffLen(byte), buff*len, code

	WriteMemString(pmask, (int)codeStream, codes, sizeof(codes));
	pExec = CreateRemoteThread(pmask->pground, NULL, 0, (LPTHREAD_START_ROUTINE)codeStream, NULL, 0, NULL);

	WaitForSingleObject(pExec, 2000);

	VirtualFreeEx(pmask->pground, vStream, 0, MEM_RELEASE);
}

void NoxServerKickClient(PMASK *pmask, char pIndex)
{
    char codes[] = { 0x55, 0x8B, 0xEC, 0x6A, 0x04, 0x6A, 0x31, 0xE8, 0xA4, 0xDA, 0xD8, 0xFF, 0x83, 0xC4, 0x08, 0x5D, 0xC3, 0x90, 0x90, 0x90 };
    char *vStream = VirtualAllocEx(pmask->pground, NULL, sizeof(codes), MEM_COMMIT, PAGE_EXECUTE_READWRITE);
    char *codeStream = vStream;

    codes[6] = pIndex;
    FixCallOpcode(codes + 7, codeStream + 7, 0x4DEAB0);
    WriteMemString(pmask, (int)codeStream, codes, sizeof(codes));
    HANDLE pExec = CreateRemoteThread(pmask->pground, NULL, 0, (LPTHREAD_START_ROUTINE)codeStream, NULL, 0, NULL);

    WaitForSingleObject(pExec, 2000);

    VirtualFreeEx(pmask->pground, vStream, 0, MEM_RELEASE);
}

