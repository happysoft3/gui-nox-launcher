#pragma once

#include "head.h"

void SubThreadRoutines(PMASK *pmask);
unsigned WINAPI SubThreadProc(PMASK *pmask);
int CreateSubThread(PMASK *pmask);
unsigned WINAPI NoxExecThread(void *arg);
unsigned WINAPI NoxLoopThread(void *arg);
