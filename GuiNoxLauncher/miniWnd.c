
#include "miniWnd.h"
#include "resource.h"


void InitMiniDlg(PMASK *pmask, HWND hDlg)
{
	RECT clientRt;

	GetWindowRect(hDlg, &clientRt);

	pmask->edit_ptr->miniWndX = clientRt.right - clientRt.left;
	pmask->edit_ptr->miniWndY = clientRt.bottom - clientRt.top + 50;

	SetWindowText(hDlg, L"녹스 미니런처 최소화모드창");

	pmask->edit_ptr->hDlgUnselBrush = CreateSolidBrush(RGB(230, 230, 230));
	pmask->edit_ptr->hDlgSelBrush = CreateSolidBrush(RGB(153, 217, 234));
	pmask->edit_ptr->minWndClick = 0;
	ShowWindow(hDlg, SW_HIDE);
}


LRESULT SetMiniWndTextColor(PMASK *pmask, WPARAM wParam)
{
	SetTextColor((HDC)wParam, RGB(156, 0, 130));
	if (pmask->edit_ptr->minWndClick)
	{
		SetBkColor((HDC)wParam, RGB(150, 210, 230));
		return (LRESULT)pmask->edit_ptr->hDlgSelBrush;
	}
	else
	{
		SetBkColor((HDC)wParam, RGB(220, 220, 220));
		return (LRESULT)pmask->edit_ptr->hDlgUnselBrush;
	}
}

void *ToggleMyDlgColor(PMASK *pmask, HWND hDlg)
{
	if (pmask->edit_ptr->minWndClick)
		return pmask->edit_ptr->hDlgSelBrush;
	else
		return pmask->edit_ptr->hDlgUnselBrush;
}

BOOL CALLBACK MiniDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	PMASK *pmask = PROC_MASK[0];
	switch (iMessage)
	{
	case WM_DESTROY:
		DeleteObject(pmask->edit_ptr->hDlgSelBrush);
		DeleteObject(pmask->edit_ptr->hDlgUnselBrush);
		return 0;
	case WM_INITDIALOG:
		InitMiniDlg(pmask, hDlg);
		return 1;
	case WM_CTLCOLORDLG:
		return (INT_PTR)ToggleMyDlgColor(pmask, hDlg);
	case WM_CTLCOLORSTATIC:
		return SetMiniWndTextColor(pmask, wParam);
	case WM_LBUTTONDOWN:
		pmask->edit_ptr->minWndClick = 1;
		InvalidateRect(hDlg, NULL, 1);
		return 0;
	case WM_LBUTTONUP:
		pmask->edit_ptr->minWndClick = 0;
		InvalidateRect(hDlg, NULL, 1);
		return 0;
	case WM_LBUTTONDBLCLK:
		ShowMainWindow(pmask, hDlg);
	}
	return 0;
}

void CreateMinimizeWindow(PMASK *pmask, HWND hWnd)
{
	if (!IsWindow(pmask->edit_ptr->miniWnd))
	{
		pmask->edit_ptr->miniWnd = CreateDialog(g_hInst, MAKEINTRESOURCE(IDD_DIALOG2), hWnd, MiniDlgProc);
	}
}

void ShowMyMinimizeWindow(PMASK *pmask)
{
	int posX = GetSystemMetrics(SM_CXSCREEN), posY = GetSystemMetrics(SM_CYSCREEN);
	int cx = pmask->edit_ptr->miniWndX, cy = pmask->edit_ptr->miniWndY;

	if (IsWindow(pmask->edit_ptr->miniWnd))
	{
		SetWindowPos(pmask->edit_ptr->miniWnd, HWND_BOTTOM, posX - cx, posY - cy, cx, cy, SWP_NOSIZE);
		ShowWindow(pmask->edit_ptr->miniWnd, SW_HIDE);
		ShowWindow(pmask->edit_ptr->main_wnd, SW_HIDE);
		AnimateWindow(pmask->edit_ptr->miniWnd, 1000, AW_BLEND);
		/*ShowWindow(pmask->edit_ptr->miniWnd, SW_SHOW);
		SetLayeredWindowAttributes(pmask->edit_ptr->miniWnd, RGB(1, 1, 1), 127, LWA_ALPHA | LWA_COLORKEY);*/
	}
}

void ShowMainWindow(PMASK *pmask, HWND hDlg)
{
	if (IsWindow(hDlg) && hDlg)
		AnimateWindow(hDlg, 500, AW_BLEND | AW_HIDE);
	ShowWindow(pmask->edit_ptr->main_wnd, SW_SHOWNORMAL);
	SetFocus(pmask->edit_ptr->main_wnd);
}