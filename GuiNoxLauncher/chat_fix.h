

#include "head.h"


#ifndef NOX_LOAD_MY_LIB
#define NOX_LOAD_MY_LIB

#define CHAT_SLOT_ADDR					0x6d8530
#define CHAT_ADDR						0x6d8534
//chat num: &addr+0x41c
#define CHAT_TOGGLE						0x6d8538
#define FIX_CHAT_ADDR					0x487eb7
#define FIX_CHAT_ADDR2					0x488396
#define FIX_IME_POS						0x200
#define SCREEN_SHOT_NAME_LEN			40
#define SH_ADDR							0x611d2c
#define REVENT_DOUBLE_RUN				0x582b50
#define HEALTH_BAR_CHECK				0x6dffd0
#define HEALTH_BAR_ADDR					0x6df368

#define CHAT_COMPLETE					0x6f8adc
#define HOST_PLAYER_ADDR				0x654284
#define SHARE_KEY1						0x716624
#define SHARE_KEY2						0x750440

#define EDIT_TXT_PTR					0x6f7b1c

typedef struct _allocDebug
{
	short mSize;
	int offset;
	int lastOffset;
}MemDebugClass;

#endif

void InitNoxChat(PMASK *pmask);
void DrawChatSlot(PMASK *pmask);
void NotifyLauncherVer(PMASK *pmask);
void FixChatBug(PMASK *pmask);
void DisplayHealthBar(PMASK *pmask);

void ChatSendConsole(PMASK *pmask, char *use_cmd);
void HookQuestModeScript(PMASK *pmask);
void AlwaysKeySharing(PMASK *pmask);
int CheckIngameChatWnd(PMASK *pmask, DWORD ptr);
void ClearNoxEditWnd(PMASK *pmask, DWORD ptr);