#pragma once

#include "head.h"

#define SUB_THREAD_TIMEOUT						5000
#define MAX_PRESERVE_LINES						10

void BeforeAppLoop(PMASK *pmask, HWND hWnd);
void LoadPortfwdModule();
void InitProcessMask(PMASK *pmask, EDIT_TXT *edit_str);

int CheckNoxPathFile(HWND hWnd);
void RunNox(HWND hWnd, PMASK *pmask, EDIT_TXT *edit_str);
void OpenNoxDirectory(PMASK *pmask);
void OpenXwisUsers(PMASK *pmask);
void OpenNaverCafe(PMASK *pmask);
int EndProgram(PMASK *pmask, HWND hWnd);
void ThisShutdown(PMASK *pmask);
void SetGameFrameLimit(PMASK *pmask, char tof);

void UnParseUserData(PMASK *pmask, int flag);
void GetPreviousUserSetting(PMASK *pmask, char *fn);
