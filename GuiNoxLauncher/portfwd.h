
#ifndef PORT_FWD_H__
#define PORT_FWD_H__

typedef struct _portFwdThreadData
{
	int *result;
	void(*fwdCb)(void);
} PortFwdThreadData;

int PortFwdCreateThread(int *result, void(*cb)(void));

#endif