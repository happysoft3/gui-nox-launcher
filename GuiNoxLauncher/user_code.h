#pragma once

#include "head.h"

#define USER_CODE_FILE_NAME					"plugins.rul"

typedef struct _pys_code
{
	unsigned char idx;
	unsigned char read_token;
	DWORD data[2];
}PARSE_CODE;


void ExecUserCodeFile(PMASK *pmask, EDIT_TXT *edit_str);

void DisplayCurTimeSetup(PMASK *pmask, int target);
void NoxScreenshotNameToTime(PMASK *pmask);
int ReadCode(PARSE_CODE *parse, char *read);