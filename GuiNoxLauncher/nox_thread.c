
#include "head.h"
#include "tools.h"
#include "gameloop.h"
#include "nox_thread.h"
#include "nox_proc.h"
#include "setGui.h"
#include "miniWnd.h"


unsigned WINAPI SubThreadProc(PMASK *pmask)
{
	while (1)
	{
		if (pmask->noxThrdEnd)
		{
			CloseHandle(pmask->noxPthread);
			pmask->noxThrdEnd = 0;
			pmask->noxPthread = NULL;
		}
		else if (!pmask->nox_exec && pmask->threadExit)
			break;
		Sleep(2);
	}
	return 0;
}

int CreateSubThread(PMASK *pmask)
{
	pmask->subThread = (HANDLE)_beginthreadex(NULL, 0, &SubThreadProc, pmask, 0, &pmask->subThdId);
	if (pmask->subThread)
		return 1;
	else
		return 0;
}

unsigned WINAPI NoxExecThread(void *arg)
{
	PMASK *pmask = arg;

	NoxSysCall(pmask);

	return 0;
}

unsigned WINAPI NoxLoopThread(void *arg)
{
	PMASK *pmask = arg;
	EDIT_TXT *edit_str = pmask->edit_ptr;
	
	if (pmask->nox_exec)
	{
		AppendText(edit_str, L"녹스 실행 준비중...\r\n");
		Sleep(3000);
		while (FindWindow(NULL, TEXT("Nox - Launch Options")) && !pmask->threadExit)
			Sleep(100);
		if (CheckNoxProcess(pmask))
		{
			AppendText(edit_str, L"녹스 실행완료\r\n");
			
			SetCurrentDirectory(pmask->cur_path);
			UpdateNoxStatTab(pmask);
			if (pmask->flag_ptr->hide_wnd)
				ShowMyMinimizeWindow(pmask);
			LoopReadProcess(pmask);
		}
		pmask->nox_exec = 0;
		CloseHandle(pmask->pground);
		AppendText(edit_str, L"녹스 종료됨\r\n");
		UpdateNoxStatTab(pmask);
		if (pmask->flag_ptr->hide_wnd)
			ShowMainWindow(pmask, edit_str->miniWnd);
	}
	pmask->noxThrdEnd = 1;
	return 0;
}

